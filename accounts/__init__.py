"""Constants for the module."""
from django.utils.translation import gettext_lazy as _

WORKER = 'worker'
SUPERUSER = 'superuser'
CUSTOMER = 'customer'
PROFILE_TYPE = {
    WORKER: _('Worker'),
    SUPERUSER: _('Superuser'),
    CUSTOMER: _('Customer'),
}

RENTER = 'renter'
BUYER = 'buyer'
PROVIDER = 'provider'
ROLES = {
    RENTER: _('Renter'),
    BUYER: _('Buyer'),
    PROVIDER: _('Provider'),
}

SUPERUSERS = 'superusers'
PURCHASES = 'purchases'
SALES = 'sales'
RENTALS = 'rentals'
WAREHOUSE = 'warehouse'
BILLING = 'billing'
ACCOUNTING = 'accounting'
GROUPS = {
    PURCHASES: _('Purchases'),
    SALES: _('Sales'),
    RENTALS: _('Rentals'),
    WAREHOUSE: _('Warehouse'),
    BILLING: _('Billing'),
    ACCOUNTING: _('Accounting'),
    SUPERUSERS: _('Superusers')
}

TZ_LIMA = 'America/Lima'
TZ_AMSTERDAM = 'Europe/Amsterdam'
TZ_NEW_YORK = 'America/New_York'
TIMEZONES = {
    TZ_LIMA: _('Lima'),
    TZ_AMSTERDAM: _('Amsterdam'),
    TZ_NEW_YORK: _('New York'),
}
SPANISH = 'es'
ENGLISH = 'en'
LANGUAGES = {
    SPANISH: _('Spanish'),
    ENGLISH: _('English'),
}

TYPE_MAIN = 'main'
TYPE_BRANCH = 'branch'
TYPE_PERSONAL = 'personal'
ADDRESS_TYPES = {
    TYPE_MAIN: _('Main'),
    TYPE_BRANCH: _('Branch'),
    # TYPE_PERSONAL: _('Personal'),
}

CAN_SEE_COMPANY_LINK = 'can_see_company_link'
CAN_SEE_WAREHOUSE_LINK = 'can_see_warehouse_link'
CAN_SEE_PURCHASES_LINK = 'can_see_purchases_link'

LINK_PERMISSIONS = {
    CAN_SEE_COMPANY_LINK: 'Can see Company link',
    CAN_SEE_WAREHOUSE_LINK: 'Can see Warehouse link',
    CAN_SEE_PURCHASES_LINK: 'Can see Purchases link'
}
