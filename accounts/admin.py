"""Used for the admin pages."""
from django.contrib import admin
from django.contrib.auth.models import Permission

from .models import Company


class CompanyAdmin(admin.ModelAdmin):
    """List of Company."""

    list_display = (
        'number',
        'name',
        'is_renter',
        'is_provider',
        'is_buyer',
        'is_active'
    )


admin.site.register(Company, CompanyAdmin)
admin.site.register(Permission)
