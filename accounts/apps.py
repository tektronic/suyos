"""Apps module."""
from django.apps import AppConfig


class AccountsConfig(AppConfig):
    """Configuration of Accounts app."""

    name = 'accounts'
