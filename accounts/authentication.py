"""Timed Authentication of tokens."""
from datetime import timedelta

from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from rest_framework.authentication import TokenAuthentication
from rest_framework import exceptions

EXPIRE_HOURS = getattr(settings, 'REST_FRAMEWORK_TOKEN_EXPIRE_HOURS')


class ExpiringTokenAuthentication(TokenAuthentication):
    """Logic to get expiring tokens."""

    def authenticate_credentials(self, key):
        """Authenticate credentials."""
        model = self.get_model()
        try:
            token = model.objects.get(key=key)
        except model.DoesNotExist:
            raise exceptions.AuthenticationFailed(_('Invalid token'))
        if not token.user.is_active:
            raise exceptions.AuthenticationFailed(
                _('Unser inactive or remove')
                )
        if token.created < timezone.now() - timedelta(hours=EXPIRE_HOURS):
            raise exceptions.AuthenticationFailed('Token has expired')
        return (token.user, token)
