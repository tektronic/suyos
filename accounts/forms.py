"""functionality for account forms."""
from django import forms
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from core.forms import AbstractModelForm, AuditableFormMixin

from . import TYPE_MAIN
from .models import Profile, Company, Address


class AddressForm(AuditableFormMixin):
    """Address form."""

    def __init__(self, *args, **kwargs):
        """Get a company or a profile to use in the Address."""
        number = kwargs.pop('number', None)
        username = kwargs.pop('username', None)
        super().__init__(*args, **kwargs)
        if number:
            self.company = Company.objects.get(number=number)
        elif username:
            self.profile = Profile.objects.get(username=username)

    def clean(self):
        """Ensure that there is only one main type.

        Note that if there is no address, then we have to create it. Otherwise,
        creation will fail.
        """
        cleaned_data = super().clean()
        has_main = self.company.addresses.filter(
            type=TYPE_MAIN,
            is_active=True
            )
        has_main = has_main.filter(~Q(id=self.instance.id))
        if cleaned_data.get('is_active') and \
           has_main and \
           cleaned_data.get('type') == TYPE_MAIN:
            raise forms.ValidationError(_(
                "Cannot have more than one Main Address."
            ))
        # get or create addresses for a model
        if not self.instance.id:
            if self.company:
                self.company.addresses.create(
                    **cleaned_data,
                    modified_by=self.profile
                    )
            elif self.profile:
                self.profile.addresses.create(
                    **cleaned_data,
                    modified_by=self.profile
                    )
        return cleaned_data

    class Meta:
        """Class Meta."""

        model = Address
        exclude = [
            'country',
            'company',
            'modified_by',
            'profile',
            'content_type',
            'object_id'
        ]


class ActorFormMixin(AbstractModelForm):
    """Actor Form. Deprecated?."""

    actor_type = forms.CharField(label=_('Actor Type'), max_length=8)

    def __init__(self, *args, **kwargs):
        """Set initial data for an actor form."""
        is_provider = kwargs.pop('provider', False)
        is_renter = kwargs.pop('renter', False)
        is_buyer = kwargs.pop('buyer', False)
        super().__init__(*args, **kwargs)
        renter_field = self.fields.get('is_renter')
        provider_field = self.fields.get('is_provider')
        buyer_field = self.fields.get('is_buyer')
        actor_type = self.fields.get('actor_type')
        if not renter_field and not provider_field and not buyer_field:
            raise AttributeError(
                _('Actor form must have "Actor Type" fields.')
                )
        renter_field.required = is_renter
        provider_field.required = is_provider
        buyer_field.required = is_buyer
        actor_type.initial = self._meta.model.get_actor_type(self.instance)
        renter_field.widget = forms.widgets.HiddenInput() if\
            not renter_field.required else renter_field.widget
        provider_field.widget = forms.widgets.HiddenInput() if\
            not provider_field.required else provider_field.widget
        buyer_field.widget = forms.widgets.HiddenInput() if\
            not buyer_field.required else buyer_field.widget


class CompanyForm(AuditableFormMixin):
    """A Form for the Company model."""

    def clean(self):
        """Ensure that there is a provider, renter or buyer."""
        cleaned_data = super().clean()
        errors = []
        if not cleaned_data.get('is_provider', False) and\
           not cleaned_data.get('is_renter', False) and\
           not cleaned_data.get('is_buyer', False):
            errors.append(forms.ValidationError(
                _('A Company must have a Type (Provider, Renter or Buyer).')
            ))
        if errors:
            raise forms.ValidationError(errors)
        return cleaned_data

    def clean_number(self):
        """Ensure that the number is bigger than min_chars."""
        number = self.cleaned_data.get('number')
        min_chars = 11
        if len(str(number)) < min_chars:
            raise forms.ValidationError(
                _('Company Number cannot be smaller than %(min_char)s'
                  'characters.'),
                params={'min_char': min_chars}
            )
        return number

    def clean_name(self):
        """Form validation for company names..."""
        name = self.cleaned_data.get('name', None)
        if len(name) < 2:
            raise forms.ValidationError(
                _('Company Name must be longer than 1'
                  'character')
                )
        return name

    class Meta:
        """Class Meta."""

        model = Company
        fields = [
            'name',
            'number',
            'is_renter',
            'is_provider',
            'is_buyer',
            'is_active'
        ]
