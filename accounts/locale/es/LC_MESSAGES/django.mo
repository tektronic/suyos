��    @        Y         �     �  7   �  "   �  ,   �  )   %  
   O     Z     b  
   i     t     �     �  	   �  	   �     �  	   �     �     �     �     �  '   �     "  	   '     1  ,   9  >   f     �     �  	   �  
   �     �  	   �     �     �     �     �               #  +   )     U     t     �     �  %   �     �     �  	   �     �     �     �     �     �     	     	     	  	   	     '	     .	  h   5	     �	     �	     �	  k  �	     C  >   Z  )   �  2   �     �            
   "     -     =     P  
   c     n     {     �     �     �     �     �  	   �  .   �       
     
   &  9   1  9   k  	   �     �  
   �     �     �     �     �  	   �     �     �          8  	   D  5   N  9   �     �     �     �  &   �       	        $     ,  
   9  
   D     O     V     ^     g     o     w  
        �  �   �          3     7     &   :                    
   +       5   3   9           %   @   "                       =   (                  ,                      .   /                  $   	            !       2   8   )       4   ?      -                    0   <                    *                #       7           6          >   1       ;   '    %(value)s must be provided. A Company must have a Type (Provider, Renter or Buyer). A client cannot belong to a group. A profile can either be a worker or an admin A user with that username already exists. Accounting Actions Active Actor Type Add Address Add Company Address Address 1 Address 2 Address Type Addresses Administrator An employee cannot be a client. Branch Buyer Cannot have more than one Main Address. City Companies Company Company Name must be longer than 1 character Company Number cannot be smaller than %(min_char)s characters. Edit English Extension First Name Language Last Name Login Main Name No Addresses available. No Companies available. Password Phone Phone number must be of the format +9999... Please login to see this page. Prefered Language Prefered Timezone Profile Profile must have at least one group. Profiles Provider Purchases Region Rentals Renter Sales Save Spanish Timezone Username Warehouse Worker Yes,No Your account doesn't have access to this page. To proceed, please login with an account that has access. Your submition has errors company numberNumber username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-13 13:36-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %(value)s es necesario Compañia debe tener tipo (Proveedor, Alquilador, o Comprador) un cliente no puede pertenecer a un grupo Un perfil tiene que ser trabajador o administrador Un usuario ya existe. Contabilidad Acciones Disponible Typo de Usuario Agregar Dirección Agregar Compañía Dirección Dirección 1 Dirección 2 Tipo de Dirección Direcciones Administrador Empleado no puede ser cliete Sucursal Comprador No pude haber mas de una dirección principal. Ciudad Compañias Compañía El nombre de la compañía debe tener mas de ún caracter número RUC no puede ser menos de %(min_char)s characters Modificar Inglés Extensión Nombre Lenguaje Apellido Iniciar Cesion Principal Nombre No hay direcciones disponibles No hay compañias disponibles Contraseña Teléfono Numero de teléfono tiene que ser de formato +9999... Por favor ingresa tus credenciales para ver esta página. Lenguaje Preferido Horario Preferido Perfil Un perfil debe tener al menos un grupo Perfiles Proveedor Compras Departamento Alquileres Alquilador Ventas Guardar Español Horario Usuario Almacen Trabajador Sí,No Tu cuenta no cuenta con los privilegios para ver esta pagina. Para continuar ingresa con una cuenta que tenga los privilegios correctos. Hay errores en los datos RUC usuario 