"""Contains constants for locations used in the system."""
from django.utils.translation import gettext as _

PERU = "pe"
UNITED_STATES = "us"
NETHERLANDS = "nl"

REGIONS = {
    PERU: {  # Peruvian regions (departamentos)
        'amazonas': _('Amazonas'),
        'ancash': _('Ancash'),
        'apurimac': _('Apurímac'),
        'arequipa': _('Arequipa'),
        'ayacucho': _('Ayacucho'),
        'cajamarca': _('Cajamarca'),
        'cusco': _('Cusco'),
        'huancavelica': _('Huancavelica'),
        'huanuco': _('Huánuco'),
        'ica': _('Ica'),
        'junin': _('Junin'),
        'la_libertad': _('La Libertad'),
        'lambayeque': _('Lambayeque'),
        'lima_region': _('Lima Region'),
        'lima_city': _('Lima City'),
        'callao': _('Callao'),
        'loreto': _('Loreto'),
        'madre_de_dios': _('Madre de Dios'),
        'moquegua': _('Moquegua'),
        'pasco': _('Pasco'),
        'puno': _('Puno'),
        'piura': _('Piura'),
        'san_martin': _('San Martin'),
        'tacna': _('Tacna'),
        'tumbes': _('Tumbes'),
        'ucayali': _('Ucayali'),
    },
    UNITED_STATES: {  # US States
        'florida': _('Florida'),
        'north_carolina': _('North Carolina'),
    },
    NETHERLANDS: {  # Netherlands regions as used in the Eropean Union
        'north': _('North Netherlands'),
        'east': _('East Netherlands'),
        'west': _('West Netherlands'),
        'south': _('South Netherlands'),
    },
}

CITIES = {
    'callao': {
        'callao': _('Callao'),
    },
    'junin': {
        'Acobamba': _('Acobamba'),
        'Palca': _('Palca'),
        'Tarma': _('Tarma'),
    },
    'lima_region': {
        'barranca': _('Barranca'),
        'paramonga': _('Paramonga'),
    },
    'lima_city': {
        'ancon': _('Ancon'),
        'ate': _('Ate'),
        'barranco': _('Barranco'),
        'breña': _('Breña'),
        'carabayllo': _('Carabayllo'),
        'chaclacayo': _('Chaclacayo'),
        'chorrillos': _('Chorrillos'),
        'cieneguilla': _('Cieneguilla'),
        'comas': _('Comas'),
        'el_agustino': _('El Agustino'),
        'independencia': _('Independencia'),
        'jesus_maria': _('Jesús María'),
        'la_molina': _('La Molina'),
        'la_victoria': _('La Victoria'),
        'lima': _('Lima'),
        'lince': _('Lince'),
        'los_olivos': _('Los Olivos'),
        'lurigancho': _('Lurigancho'),
        'lurin': _('Lurín'),
        'magdalena_del_mar': _('Magdalena del Mar'),
        'miraflores': _('Miraflores'),
        'pachacamac': _('Pachacamac'),
        'pucusana': _('Pucusana'),
        'pueblo_libre': _('Pueblo Libre'),
        'puente_piedra': _('Puente Piedra'),
        'punta_hermosa': _('Punta Hermosa'),
        'punta_negra': _('Punta Negra'),
        'rimac': _('Rimac'),
        'san_bartolo': _('San Bartolo'),
        'san_borja': _('San Borja'),
        'san_isidro': _('San Isidro'),
        'san_juan_de_lurigancho': _('San Juan de Lurigancho'),
        'san_juan_de_miraflores': _('San Juan de Miraflores'),
        'san_luis': _('San Luis'),
        'san_martin_de_porres': _('San Martin de Porres'),
        'san_miguel': _('San Miguel'),
        'santa_anita': _('Santa Anito'),
        'santa_maria_del_mar': _('Santa Maria del Mar'),
        'santa_rosa': _('Santa Rosa'),
        'santiago_de_surco': _('Santiago de Surco'),
        'surquillo': _('Surquillo'),
        'villa_el_salvador': _('Villa el Salvador'),
        'villa_maria_del_triunfo': _('Villa Maria del Triunfo'),
    },
    'piura': {
        'ayabaca': _('Ayabaca'),
        'piura': _('Piura'),
        'castilla': _('Castilla'),
        'catacaos': _('Catacaos'),
        'las_lomas': _('Las Lomas'),
        'tambogrande': _('Tambogrande'),
        'sullana': _('Sullana'),
        'bellavista': _('Bellavista'),
        'marcavelica': _('Marcavelica'),
        'lancones': _('Lancones'),
        'querecotillo': _('Querecotillo'),
        'salitral': _('Salitral'),
    },
    'tumbes': {
        'zorritos': _('Zorritos'),
        'punta_sal': _('Punta Sal'),
        'tumbes': _('Tumbes'),
        'papayal': _('Papayal'),
        'zarumilla': _('Zarumilla'),
        'aguas_verdes': _('Aguas Verdes'),
    },
    'lambayeque': {
        'chiclayo': _('Chiclayo'),
    }
}


COUNTRIES = {
    PERU: _('Peru'),
    # UNITED_STATES : _('United States'),
    # NETHERLANDS : _('Netherlands'),
}
PERUVIAN_DEPARTMENTS = REGIONS['pe']
PERUVIAN_CITIES = {
    **CITIES['callao'],
    **CITIES['junin'],
    **CITIES['lima_city'],
    **CITIES['lima_region'],
    **CITIES['piura'],
    **CITIES['tumbes'],
    **CITIES['lambayeque']
}
