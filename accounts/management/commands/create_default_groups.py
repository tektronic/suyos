"""Create default groups for app."""
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

from accounts import GROUPS


def create_groups():
    """Get or creates groups for the system."""
    for k in GROUPS:
        g, created = Group.objects.get_or_create(name=k)


class Command(BaseCommand):
    """Command for creating groups."""

    help = 'creates groups'

    def handle(self, *args, **kwargs):
        """Create groups."""
        create_groups()
