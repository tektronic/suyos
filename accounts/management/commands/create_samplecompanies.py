"""Create sample companies."""
from django.core.management.base import BaseCommand

from accounts import TYPE_MAIN, TYPE_BRANCH
from accounts.models import Profile, Company


class Command(BaseCommand):
    """Creates sampel companies with addresses.

    Requires to have sample users.
    """

    def handle(self, *args, **kwargs):
        """Create Companies."""
        p = Profile.objects.get(username='twarehouse')
        c = Company.objects.create(
            name='Grupo Deltron S.A.',
            number='20212331377',
            is_provider=True,
            modified_by=p
            )
        c.addresses.create(
            type=TYPE_MAIN,
            address1='Raul Rebagliati 170',
            address2='Urb. Santa Catalina',
            region='lima_city',
            city='la_victoria',
            country='pe',
            email='ventas@deltron.com.pe',
            phone='+5114150101',
            modified_by=p
            )
        c.addresses.create(
            type=TYPE_BRANCH,
            address1='Av Garcilazo De La Vega 1251',
            region='lima_city',
            city='lima',
            country='pe',
            email='ventas@deltron.com.pe',
            phone='+511307308',
            modified_by=p
            )
        c.addresses.create(
            type=TYPE_BRANCH,
            address1='Av Salaverry 580',
            address2='Urb. Patazca',
            region='lambayeque',
            city='chiclayo',
            country='pe',
            email='ventas@deltron.com.pe',
            phone='+5174205346',
            modified_by=p
            )
        c = Company.objects.create(
            name='Tektronic E.I.R.L.',
            number='20509895776',
            modified_by=p
            )
        c.addresses.create(
            type=TYPE_MAIN,
            address1='Av Arenales 646',
            region='lima_city',
            city='jesus_maria',
            country='pe',
            email='ventas@tektronickperu.com',
            phone='+5114333495',
            modified_by=p
            )
        c.addresses.create(
            type=TYPE_BRANCH,
            address1='Av Arenales 648',
            region='lima_city',
            city='jesus_maria',
            country='pe',
            email='ventas@tektronickperu.com',
            phone='+5114315911',
            modified_by=p
            )
