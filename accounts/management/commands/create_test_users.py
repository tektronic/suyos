"""Commands used in the creation of test data."""
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

from accounts import (
    GROUPS,
    SUPERUSERS,
    TZ_AMSTERDAM,
    TZ_LIMA,
    SPANISH,
    ENGLISH,
    SUPERUSER,
    WORKER,
)
from accounts.models import Profile


def create_profiles():
    """Create users for groups."""
    password = 'password'
    for g in GROUPS:
        try:
            group = Group.objects.get(name=g)
        except Group.DoesNotExist:
            print(
                "{} does not exist in the system. You may need to create"
                "Groups first".format(g)
                )
        else:
            if g is SUPERUSERS:
                Profile.objects.get_or_create_profile(
                    'manton',
                    'aP455word',
                    profile_type=SUPERUSER,
                    first_name='Marco',
                    last_name='Anton',
                    timezone=TZ_AMSTERDAM,
                    language=ENGLISH
                    )
            else:
                username = "{}{}".format('t', group.name)
                Profile.objects.get_or_create_profile(
                    username,
                    password,
                    profile_type=WORKER,
                    group=group,
                    first_name=group.name.title(),
                    last_name='Test',
                    timezone=TZ_LIMA,
                    language=SPANISH
                )


class Command(BaseCommand):
    """Excecute commands for application."""

    help = 'Create Users for testing'

    def handle(self, *args, **options):
        """Create users for testing."""
        create_profiles()
