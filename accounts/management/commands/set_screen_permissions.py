"""Set screen permissions."""
from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand

from accounts import (
    GROUPS,
    SUPERUSERS,
    WAREHOUSE,
    PURCHASES,
    CAN_SEE_COMPANY_LINK,
    CAN_SEE_WAREHOUSE_LINK,
    CAN_SEE_PURCHASES_LINK,
    LINK_PERMISSIONS
)


def create_link_permissions():
    """Generate screen permissions. Add more if necesary."""
    company_app, company_model = settings.COMPANY_MODEL.lower().split('.')
    warehouse_app, warehouse_model = \
        settings.WAREHOUSE_PRODUCT_MODEL.lower().split('.')
    purchases_app, purchases_model = \
        settings.PURCHASES_BILL_MODEL.lower().split('.')
    company_ct = ContentType.objects.get(
        app_label=company_app,
        model=company_model
    )
    bill_ct = ContentType.objects.get(
        app_label=purchases_app,
        model=purchases_model
    )
    product_ct = ContentType.objects.get(
        app_label=warehouse_app,
        model=warehouse_model
    )
    Permission.objects.get_or_create(
        name=LINK_PERMISSIONS[CAN_SEE_COMPANY_LINK],
        content_type=company_ct,
        codename=CAN_SEE_COMPANY_LINK
    )
    Permission.objects.get_or_create(
        name=LINK_PERMISSIONS[CAN_SEE_PURCHASES_LINK],
        content_type=bill_ct,
        codename=CAN_SEE_PURCHASES_LINK
    )
    Permission.objects.get_or_create(
        name=LINK_PERMISSIONS[CAN_SEE_WAREHOUSE_LINK],
        content_type=product_ct,
        codename=CAN_SEE_WAREHOUSE_LINK
    )


def assign_screen_permissions(group):
    """Assign special permissions to groups."""
    try:
        company_permission = Permission.objects.get(
            codename=CAN_SEE_COMPANY_LINK
            )
        purchases_permission = Permission.objects.get(
            codename=CAN_SEE_PURCHASES_LINK
            )
        warehouse_permissions = Permission.objects.get(
            codename=CAN_SEE_WAREHOUSE_LINK
            )
        update_company = Permission.objects.get(
            codename='change_company'
            )
        add_company = Permission.objects.get(codename='add_company')
        add_address = Permission.objects.get(codename='add_address')
        update_address = Permission.objects.get(
            codename='change_address'
            )
    except Permission.DoesNotExist as error:
        print(
            'The following exception was raised when trying to '
            'retrieve permissions {}'.format(repr(error))
        )
    else:
        if group.name is WAREHOUSE:
            group.permissions.add(
                company_permission,
                warehouse_permissions,
                update_company,
                add_address,
                update_address,
            )
        elif group.name is PURCHASES:
            group.permissions.add(
                company_permission,
                purchases_permission,
                add_company,
                update_company,
                add_address,
                update_address,
            )


def assign_permissions_to_groups():
    """Assign permissions to groups."""
    for group_string in GROUPS:
        try:
            group = Group.objects.get(name=group_string)
        except Group.DoesNotExist:
            print(
                "{} is not a group in the system. Try adding "
                "it.".format(group_string))
        else:
            if group_string is not SUPERUSERS:
                content_types = ContentType.objects.filter(
                    app_label=group_string
                    )
                permissions = Permission.objects.filter(
                    content_type__in=content_types
                    )
            else:
                permissions = Permission.objects.all()
            # set permissions to a group
            group.permissions.set(permissions)
            assign_screen_permissions(group)


class Command(BaseCommand):
    """Set screen permissions."""
    help = 'create screen permissions. Remember to call init_auth before'

    def handle(self, *args, **kwargs):
        """Command to set screen permissions."""
        create_link_permissions()
        assign_permissions_to_groups()
