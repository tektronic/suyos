"""Models for the account app."""
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import (
    GenericForeignKey,
    GenericRelation
    )
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin, Group
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext as _

from core.models import Auditable

from . import (
    TIMEZONES,
    LANGUAGES,
    SPANISH,
    TZ_LIMA,
    ADDRESS_TYPES,
    RENTER,
    BUYER,
    PROVIDER,
    ROLES,
    SUPERUSERS,
    PROFILE_TYPE,
    WORKER,
    SUPERUSER,
    CUSTOMER
    )
from .locations import COUNTRIES, PERUVIAN_DEPARTMENTS, PERUVIAN_CITIES, PERU


class Address(Auditable):
    """Models an address.

    Adrresses can be bounded to different models through the use of
    ContnetTypes. Currently used in companies and profiles.
    """

    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message=_("Phone number must be of the format +9999...")
        )

    # Need to make the following 2 nullable.
    content_type = models.ForeignKey(
        ContentType,
        related_name='addresses',
        on_delete=models.CASCADE,
        default='',
        )
    object_id = models.IntegerField(default=-1)
    content_object = GenericForeignKey()

    type = models.CharField(
        max_length=10,
        choices=sorted(ADDRESS_TYPES.items())
        )
    is_active = models.BooleanField(default=True)
    address1 = models.CharField(max_length=50)
    address2 = models.CharField(max_length=50, blank=True)
    country = models.CharField(
        max_length=2,
        choices=sorted(COUNTRIES.items()),
        default=PERU,
        )
    region = models.CharField(
        max_length=20,
        choices=sorted(PERUVIAN_DEPARTMENTS.items())
        )
    city = models.CharField(
        max_length=30,
        choices=sorted(PERUVIAN_CITIES.items())
        )
    zip_code = models.CharField(max_length=6, blank=True)
    phone = models.CharField(
        validators=[phone_regex],
        max_length=17,
        blank=True
        )
    extension = models.CharField(max_length=6, blank=True)
    fax = models.CharField(max_length=12, blank=True)
    email = models.CharField(max_length=50, blank=True)

    def delete(self):
        """On deletion, it sets the the active flag to false."""
        self.active = False
        self.save()

    def __str__(self):
        """Format an address for printing."""
        display_list = [self.address1]
        display_list.append(self.address2) if self.address2 else None
        display_list.append(self.zip_code) if self.zip_code else None
        display_list.extend([
            PERUVIAN_CITIES[self.city],
            PERUVIAN_DEPARTMENTS[self.region],
        ])
        return ", ".join(display_list)

    class Meta:
        """Metaclass."""

        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')


class ActorManager(models.Manager):
    """Manager for the Actor model.

    If the data structure grows, consider adding the actor_type column to the
    database beware however, that translations would not be avilable...
    TODO: may not need it anymore...
    """

    def all(self, *args, **kwargs):
        """Populate actor type in all the models."""
        results = super(ActorManager, self).all(*args, **kwargs)
        for item in results:
            item.actor_type = self.model.get_actor_type(item)
        return results

    def get(self, *args, **kwargs):
        """Set actor_type when getting Actor elements."""
        item = super(ActorManager, self).get(*args, **kwargs)
        item.actor_type = self.model.get_actor_type(item)
        return item

    def filter(self, *args, **kwargs):
        """Set the Actor type when filtering elements."""
        results = super(ActorManager, self).filter(*args, **kwargs)
        for item in results:
            item.actor_type = self.model.get_actor_type(item)
        return results


class Actor(models.Model):
    """Set flags for any actor of they system.

    TODO:
    * check if type is better than flags. Take into consideration translations.
    * Also consider that an actor can have multiple roles, so the potential
      type would have to be a coma separated list.
    """

    is_renter = models.BooleanField(default=False)
    is_provider = models.BooleanField(default=False)
    is_buyer = models.BooleanField(default=False)

    objects = ActorManager()

    def get_actor_type(self):
        """
        Get a string that represents the type of role an actor might have.

        TODO: update this to a property and dont use a manager?
        """
        types = []
        if self.is_renter:
            types.append(str(ROLES[RENTER]))
        if self.is_provider:
            types.append(str(ROLES[PROVIDER]))
        if self.is_buyer:
            types.append(str(ROLES[BUYER]))
        return ', '.join(types)

    class Meta:
        """Abstract class."""

        abstract = True


class Company(Auditable, Actor):
    """models a company in the system."""

    number = models.CharField(max_length=11, unique=True)
    name = models.CharField(max_length=150, unique=True)
    is_active = models.BooleanField(default=True)
    addresses = GenericRelation(Address)

    def __str__(self):
        """Representation of a Company."""
        return '{}'.format(self.name.title())

    def delete(self, *args, **kwargs):
        """Set flag to false when deleting."""
        self.is_active = False
        self.save(*args, **kwargs)

    class Meta:
        """Metaclass."""

        verbose_name = _('Company')
        verbose_name_plural = _('Companies')


class ProfileManager(BaseUserManager):
    """Manager of the profile Model.

    mainly used for create actions.
    """

    use_in_migrations = True

    def _create_profile(self, username, password, **kwargs):
        """Create and save a User.

        Should be used by all methods that try to create a user.
        """
        if not username:
            raise ValueError('The given username must be set')
        username = self.model.normalize_username(username)
        user = self.model(username=username, **kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def get_or_create_profile(self, username, password, **kwargs):
        """Get or create a profile.

        if the username does not exists, it will create a profile depending
        on the profile_type. If the username exists, then it will return
        the newly created profile. Beware that it will search a profile with
        all the kwargs given.

        TODO:
            * catch create a profile with an already existing username?

        Returns:
            A tupple containing a profile and whether it was created or not.

        """
        profile_type = kwargs.get('profile_type', None)
        group = kwargs.pop('group', None)
        created = False
        try:
            profile = Profile.objects.get(
                username=username,
                password=password,
                **kwargs
                )
        except Profile.DoesNotExist:
            if profile_type == WORKER and group:
                profile = self.create_worker_profile(
                    username=username,
                    password=password,
                    group=group,
                    **kwargs
                    )
                created = True
            elif profile_type == SUPERUSER:
                profile = self.create_superuser(
                    username=username,
                    password=password,
                    **kwargs
                    )
                created = True
            elif profile_type == CUSTOMER:
                profile = self.create_customer_profile(
                    username=username,
                    password=password,
                    **kwargs
                    )
                created = True
            else:
                raise ValueError(_('Profile type is not allowed.'))
        return (profile, created)

    def create_customer_profile(self, username, password, **kwargs):
        """Create a customer profile."""
        return self._create_profile(username, password, **kwargs)

    def create_worker_profile(self, username, password, group, **kwargs):
        """Create a worker profile. must have a group."""
        if not group:
            raise ValueError(_('Profile must have a group!'))
        profile = self._create_profile(username, password, **kwargs)
        group.user_set.add(profile) if profile else None
        return profile

    def create_superuser(self, username, password, **kwargs):
        """Create a superuser.

        if there is no group superuser, this method will create it.
        """
        kwargs.setdefault('is_superuser', True)
        g, _ = Group.objects.get_or_create(name=SUPERUSERS)
        profile = self._create_profile(username, password, **kwargs)
        g.user_set.add(profile) if profile else None
        return profile


class Profile(AbstractBaseUser, Actor, PermissionsMixin):
    """Users of the system. Only these users are allowed to login."""

    _error_codes = {
        'isp': 'invalid staff profile',
        'icp': 'invalid client profile',
        'mrv': 'missing required value',
        }
    _messages = {
        'staff_isno_client': _('An employee cannot be a client.'),
        'staff_hasnot_group': _('Profile must have at least one group.'),
        'client_has_group': _('A client cannot belong to a group.'),
        'missing_value': _('%(value)s must be provided.'),
        'worker_or_admin': _('A profile can either be a worker or an admin')
        }

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        error_messages={
            'unique': _("A user with that username already exists.")
            },
        )
    first_name = models.CharField(
        _('First Name'),
        max_length=50,
        blank=True
        )
    last_name = models.CharField(_('Last Name'), max_length=50, blank=True)
    # Used for the admin site.
    is_staff = models.BooleanField(_('Administrator'), default=False)
    profile_type = models.CharField(
        _('Type'),
        max_length=10,
        default=CUSTOMER,
        choices=sorted(PROFILE_TYPE.items())
        )
    is_active = models.BooleanField(_('Active'), default=True)
    timezone = models.CharField(
        _('Prefered Timezone'),
        max_length=50,
        default=TZ_LIMA,
        choices=sorted(TIMEZONES.items())
        )
    language = models.CharField(
        _('Prefered Language'),
        max_length=50,
        default=SPANISH,
        choices=sorted(LANGUAGES.items())
        )
    company = models.ForeignKey(
        Company,
        null=True,
        default=None,
        on_delete=models.DO_NOTHING
        )
    addresses = GenericRelation(Address)

    objects = ProfileManager()

    USERNAME_FIELD = 'username'

    class Meta:
        """Meta."""

        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')

    def get_full_name(self):
        """Return the first_name plus the last_name with a space in between."""
        full_name = '{} {}'.format(self.first_name, self.last_name)
        return full_name.strip()

    def delete(self, *args, **kwargs):
        """On deletion of an address, set the is_active flag to false."""
        self.is_active = False
        self.save(*args, **kwargs)

    @staticmethod
    def get_new_username(first_names, last_names):
        """Generate a new username for a profile.

        Parameters:
            :first_names: first names of for a profile.
            :last_name: last names for a profile
        Returns:
            A username containing the first letter of the first name plus
            the last name. Example 'John Doe's username is jdoe or jdoeX
            where X is a number for users that have the same username' or
            None if no first_name and last_name are given.

        """
        username = None
        if first_names and last_names:
            first_names = list(map(str.lower, first_names.split(' ')))
            last_names = list(map(str.lower, last_names.split(' ')))
            letter_first_name = first_names[0][0]  # first letter of first name
            last_name = last_names[0]
            username = letter_first_name + last_name
            profiles = Profile.objects.filter(
                username__startswith=username
                ).order_by('-username')
            if profiles:
                # assume that therer is only one digit...
                # TODO: update to support multiple digits...
                try:
                    number = int(profiles.first().username[-1])
                except ValueError:
                    number = 0
                username += str(number + 1)
        return username

    def get_short_name(self):
        """Get the short name of a user. aka username..."""
        return self.username

    def clean(self):
        """Prepare user to be saved.

        Rework this... not really used...
        """
        errors = []
        self.username = self.get_new_username(self.first_name, self.last_name)
        if not self.username:
            errors.append(ValidationError(
                self._messages.get('missing_value'),
                code=self._error_codes.get('mrv'),
                params={'values': [_('First Name'), _('Last Name')]}
            ))
        if errors:
            raise ValidationError(errors)

    def __str__(self):
        """Represenation of a profile (username)."""
        return self.username
