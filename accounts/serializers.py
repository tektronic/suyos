"""Serializers for django rest_framework."""
from rest_framework.serializers import ModelSerializer

from .models import Company, Address


class AddressSerializer(ModelSerializer):
    """Serializing an Address."""

    class Meta:
        """Model and fields to be serialized."""

        model = Address
        fields = (
            'type',
            'address1',
            'address2',
            'region',
            'city',
            'phone',
            'email',
        )


class CompanySerializer(ModelSerializer):
    """Serializes a Company."""

    class Meta:
        """Model and fields to include in the serializer."""

        model = Company
        fields = (
            'number',
            'name',
            # 'addresses',
        )
