"""Test Forms."""
from django.test import TestCase

from core.tests.test_forms import FormMixinTestCase

from ..forms import ActorFormMixin
from ..models import Actor


class ActorFormMixinTestCase(FormMixinTestCase):
    """Test Actor forms."""

    mixin = Actor

    def test_init(self):
        """Test wether init sets the correct visibility of actor flags."""
        with self.assertRaises(AttributeError):
            ActorFormMixin(model=self.model)
        # for some reason, the form doesn't create the correct fields...
        # data = {'buyer':True}
        # f = ActorFormMixin(model=self.model, data=data)
        # self.assertFalse(f.fields['is_provider'].required)
        # self.asserEqual(f.fields['is_provider'].widget, HiddenInput())


class CompanyFormTest(TestCase):
    """Tests a company form."""

    def test_actor_init_fields(self):
        """Check if the correct behavior of an actor is maintained.

        TODO:
        * move to the ActorFormMixinTestCase if possible...
        * uncomment the widgets... CompanyForm is no longer inheriting from
          ActorForm hence the actor fields (renter, provider, buyer) are no
          longer hidden fields and no longer not required...
        """
        '''
        f = CompanyForm()
        renter_field = f.fields['is_renter']
        provider_field = f.fields['is_provider']
        buyer_field = f.fields['is_buyer']
        self.assertFalse(renter_field.required)
        self.assertEqual(renter_field.widget.__class__, HiddenInput)
        self.assertFalse(provider_field.required)
        self.assertEqual(provider_field.widget.__class__, HiddenInput)
        self.assertFalse(buyer_field.required)
        self.assertEqual(buyer_field.widget.__class__, HiddenInput)
        f = CompanyForm(provider= True)
        renter_field = f.fields['is_renter']
        provider_field = f.fields['is_provider']
        buyer_field = f.fields['is_buyer']
        self.assertFalse(renter_field.required)
        self.assertEqual(renter_field.widget.__class__, HiddenInput)
        self.assertTrue(provider_field.required)
        self.assertNotEqual(provider_field.widget.__class__, HiddenInput)
        self.assertFalse(buyer_field.required)
        self.assertEqual(buyer_field.widget.__class__, HiddenInput)
        f = CompanyForm(renter= True)
        renter_field = f.fields['is_renter']
        provider_field = f.fields['is_provider']
        buyer_field = f.fields['is_buyer']
        self.assertTrue(renter_field.required)
        self.assertNotEqual(renter_field.widget.__class__, HiddenInput)
        self.assertFalse(provider_field.required)
        self.assertEqual(provider_field.widget.__class__, HiddenInput)
        self.assertFalse(buyer_field.required)
        self.assertEqual(buyer_field.widget.__class__, HiddenInput)
        f = CompanyForm(buyer= True)
        renter_field = f.fields['is_renter']
        provider_field = f.fields['is_provider']
        buyer_field = f.fields['is_buyer']
        self.assertFalse(renter_field.required)
        self.assertEqual(renter_field.widget.__class__, HiddenInput)
        self.assertFalse(provider_field.required)
        self.assertEqual(provider_field.widget.__class__, HiddenInput)
        self.assertTrue(buyer_field.required)
        self.assertNotEqual(buyer_field.widget.__class__, HiddenInput)
        f = CompanyForm(provider=True, buyer= True)
        renter_field = f.fields['is_renter']
        provider_field = f.fields['is_provider']
        buyer_field = f.fields['is_buyer']
        self.assertFalse(renter_field.required)
        self.assertEqual(renter_field.widget.__class__, HiddenInput)
        self.assertTrue(provider_field.required)
        self.assertNotEqual(provider_field.widget.__class__, HiddenInput)
        self.assertTrue(buyer_field.required)
        self.assertNotEqual(buyer_field.widget.__class__, HiddenInput)
        f = CompanyForm(provider=True, renter= True)
        renter_field = f.fields['is_renter']
        provider_field = f.fields['is_provider']
        buyer_field = f.fields['is_buyer']
        self.assertTrue(renter_field.required)
        self.assertNotEqual(renter_field.widget.__class__, HiddenInput)
        self.assertTrue(provider_field.required)
        self.assertNotEqual(provider_field.widget.__class__, HiddenInput)
        self.assertFalse(buyer_field.required)
        self.assertEqual(buyer_field.widget.__class__, HiddenInput)
        f = CompanyForm(buyer=True, renter= True)
        renter_field = f.fields['is_renter']
        provider_field = f.fields['is_provider']
        buyer_field = f.fields['is_buyer']
        self.assertTrue(renter_field.required)
        self.assertNotEqual(renter_field.widget.__class__, HiddenInput)
        self.assertFalse(provider_field.required)
        self.assertEqual(provider_field.widget.__class__, HiddenInput)
        self.assertTrue(buyer_field.required)
        self.assertNotEqual(buyer_field.widget.__class__, HiddenInput)
        '''
