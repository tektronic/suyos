"""Test accounts app.

TODO: Need to test addresses....
"""
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils.translation import ugettext_lazy as _

from core.tests.test_models import ModelMixinTestCase

from .. import (
    TZ_LIMA,
    SPANISH,
    RENTER,
    BUYER,
    PROVIDER,
    ROLES,
    CUSTOMER,
    )
from ..models import Actor, Company, Profile


class ActorTestCase(ModelMixinTestCase):
    """Test the actor model."""

    mixin = Actor

    def setUp(self):
        """Create an actor based on the mixin."""
        self.actor = self.model.objects.create()

    def test_actor_fields(self):
        """Test actor's fields."""
        is_renter_field = self.actor._meta.get_field('is_renter')
        label = is_renter_field.verbose_name
        default = is_renter_field.default
        self.assertEqual(label, 'is renter')
        self.assertEqual(default, False)

        is_provider_field = self.actor._meta.get_field('is_provider')
        label = is_provider_field.verbose_name
        default = is_provider_field.default
        self.assertEqual(label, 'is provider')
        self.assertEqual(default, False)

        is_buyer_field = self.actor._meta.get_field('is_buyer')
        label = is_buyer_field.verbose_name
        default = is_buyer_field.default
        self.assertEqual(label, 'is buyer')
        self.assertEqual(default, False)

    def test_actor_type(self):
        """Test the get actor type method."""
        a_type = self.actor.get_actor_type()
        self.assertEqual(a_type, '')
        self.actor.is_renter = True
        a_type = self.actor.get_actor_type()
        self.assertEqual(a_type, ROLES[RENTER])
        self.actor.is_buyer = True
        a_type = self.actor.get_actor_type()
        self.assertEqual(a_type, '{}, {}'.format(ROLES[RENTER], ROLES[BUYER]))
        self.actor.is_provider = True
        a_type = self.actor.get_actor_type()
        self.assertEqual(a_type, '{0}, {2}, {1}'.format(
            ROLES[RENTER],
            ROLES[BUYER],
            ROLES[PROVIDER]
        ))

    def test_get_actor(self):
        """Test the get from the actor manager."""
        self.actor.is_renter = True
        self.actor.save()
        try:
            a = self.model.objects.get(pk=1)
        except Exception as error:
            self.fail(
                "Actor manager raised an Unexpected Exception when calling "
                "get: {}".format(repr(error))
                )
        self.assertEqual(a.actor_type, ROLES[RENTER])

    def test_filter_actor(self):
        """Tests the filter from the actor manager."""
        self.actor.is_renter = True
        self.actor.save()
        self.model.objects.create(is_renter=True)
        try:
            results = self.model.objects.filter(is_renter=True)
        except Exception as error:
            self.fail(
                "Actor manager raised an Unexpected Exception when calling"
                "filter: {}".format(repr(error))
                )
        self.assertEqual(len(results), 2)

    def test_all_actor(self):
        """Test the all from actor."""
        try:
            results = self.model.objects.all()
        except Exception as error:
            self.fail(
                "Actor manager raised an Unexpected Exception when calling "
                "all(): {}".format(repr(error))
                )
        self.model.objects.create()
        self.model.objects.create()
        results = self.model.objects.all()
        self.assertEqual(len(results), 3)


class CompanyTestCase(TestCase):
    """Test a company model."""

    user_model = get_user_model()

    def setUp(self):
        """Create a user for the testcase."""
        self.profile = self.user_model.objects.create_superuser(
            'cuser',
            'password'
            )

    def test_company_label(self):
        """Test company labels."""
        company = Company.objects.create(
            created_by=self.profile, number=1, name='Company Test'
        )
        number_field = company._meta.get_field('number')
        label = number_field.verbose_name
        unique = number_field.unique
        self.assertEqual(label, 'number')
        self.assertEqual(unique, True)

        name_field = company._meta.get_field('name')
        label = name_field.verbose_name
        max_length = name_field.max_length
        self.assertEqual(label, 'name')
        self.assertEqual(max_length, 150)

        is_active_field = company._meta.get_field('is_active')
        label = is_active_field.verbose_name
        default = is_active_field.default
        self.assertEqual(label, 'is active')
        self.assertEqual(default, True)

    def test_str(self):
        """Test string representation of a company."""
        cu = self.profile
        company = Company.objects.create(
            created_by=cu, number=1, name='Company Test'
        )
        self.assertEqual(str(company), 'Company Test')

    def test_delete(self):
        """Ensure that delete, only deactivates an company."""
        cu = self.profile
        mu = self.user_model.objects.create_superuser(
            'mu',
            'password'
            )
        company = Company.objects.create(
            created_by=cu, number=1, name='Company Test'
        )
        # TODO: Update test when proper solution is found for auditables...
        # with self.assertRaises(company.MissingProfileException):
        #     company.delete()
        company.delete(modified_by=mu)
        self.assertEqual(company.is_active, False)


class ProfileTestCase(TestCase):
    """Test Profile functionality."""

    def test_profile_fields(self):
        """Ensure that Profile has the correct fields and labels."""
        profile = Profile.objects.create_superuser(
            'tuser',
            'password'
            )
        singular = profile._meta.verbose_name
        plural = profile._meta.verbose_name_plural
        self.assertEqual(singular, _('Profile'))
        self.assertEqual(plural, _('Profiles'))

        profile_field = profile._meta.get_field('first_name')
        label = profile_field.verbose_name
        max_length = profile_field.max_length
        blank = profile_field.blank
        self.assertEqual(label, _('First Name'))
        self.assertEqual(max_length, 50)
        self.assertEqual(blank, True)

        profile_field = profile._meta.get_field('last_name')
        label = profile_field.verbose_name
        max_length = profile_field.max_length
        blank = profile_field.blank
        self.assertEqual(label, _('Last Name'))
        self.assertEqual(max_length, 50)
        self.assertEqual(blank, True)

        profile_field = profile._meta.get_field('is_staff')
        label = profile_field.verbose_name
        default = profile_field.default
        self.assertEqual(label, _('Administrator'))
        self.assertEqual(default, False)

        profile_field = profile._meta.get_field('profile_type')
        label = profile_field.verbose_name
        default = profile_field.default
        self.assertEqual(label, _('Type'))
        self.assertEqual(default, CUSTOMER)

        profile_field = profile._meta.get_field('is_active')
        label = profile_field.verbose_name
        default = profile_field.default
        self.assertEqual(label, _('Active'))
        self.assertEqual(default, True)

        profile_field = profile._meta.get_field('timezone')
        label = profile_field.verbose_name
        default = profile_field.default
        max_length = profile_field.max_length
        self.assertEqual(label, _('Prefered Timezone'))
        self.assertEqual(default, TZ_LIMA)
        self.assertEqual(max_length, 50)

        profile_field = profile._meta.get_field('language')
        label = profile_field.verbose_name
        default = profile_field.default
        max_length = profile_field.max_length
        self.assertEqual(label, _('Prefered Language'))
        self.assertEqual(default, SPANISH)
        self.assertEqual(max_length, 50)

    def test_full_name(self):
        """Test fullname function."""
        profile = Profile.objects.create_superuser(
            username='tuser',
            password='password',
            first_name='Test',
            last_name='User'
        )
        self.assertEqual(profile.get_full_name(), 'Test User')

    def test_delete(self):
        """Ensure that deletes sets is_active to false."""
        profile = Profile.objects.create_superuser(
            'user',
            'password'
            )
        profile.delete()
        self.assertEqual(profile.is_active, False)

    def test_get_username(self):
        """Tests get_username method.

        TODO: check if it is a static method.
        """
        # for the amount of arguments given.
        with self.assertRaises(TypeError):
            Profile.get_new_username()
        self.assertEqual(Profile.get_new_username(None, None), None)
        self.assertEqual(Profile.get_new_username('First', 'Last'), 'flast')
        # check with multiple last names
        self.assertEqual(
            Profile.get_new_username('F F N', 'Last Name'),
            'flast'
            )
        # check if it returns usernames with a digit.
        Profile.objects.create_superuser('flast', 'password')
        self.assertEqual(Profile.get_new_username('Foo', 'Last'), 'flast1')
        Profile.objects.create_superuser('flast1', 'password')
        self.assertEqual(
            Profile.get_new_username('Foo', 'Last'),
            'flast2'
            )
        # make sure that flast2 does not exist and that get_username returns
        # a 'flast4' as a username when flast3 is created.
        Profile.objects.create_superuser('flast3', 'password')
        with self.assertRaises(Profile.DoesNotExist):
            self.assertFalse(Profile.objects.get(username='flast2'))
        self.assertEqual(Profile.get_new_username('Foo', 'Last'), 'flast4')
