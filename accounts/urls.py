"""Urls used in the accounts app."""
from django.urls import include, path

from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter

from .views import (
    _LoginView,
    _LogoutView,
    CompanyListView,
    CompanyCreateView,
    CompanyUpdateView,
    AddressListView,
    AddressCreateView,
    AddressUpdateView,
    CompanyViewSet,
    AddressListAPIView,
    ObtainExpiringAuthToken,
)

app_name = 'accounts'

urlpatterns = [
    path(
        'login/',
        _LoginView.as_view(
            template_name='accounts/login.html',
            extra_context={'login_active': 'active'}
            ),
        name='login'
        ),
    path('logout/', _LogoutView.as_view(next_page='core:home'), name='logout'),
    path('companies/', CompanyListView.as_view(), name='companies'),
    path('companies/add/', CompanyCreateView.as_view(), name='company_add'),
    path(
        'companies/<int:number>/',
        CompanyUpdateView.as_view(),
        name='company_update'
        ),
    path(
        'companies/<int:number>/addresses/',
        AddressListView.as_view(),
        name='company_addresses'
        ),
    path(
        'companies/<int:number>/address/add/',
        AddressCreateView.as_view(),
        name='company_address_add'
        ),
    path(
        'companies/<int:number>/address/<int:pk>/',
        AddressUpdateView.as_view(),
        name='company_address_update'
        ),
]

router = DefaultRouter(trailing_slash=False)
router.register(r'api/companies', CompanyViewSet)

api_viewsets = [
    path('', include(router.urls)),
    ]
api_urlpatterns = format_suffix_patterns([
    path(
        'api/companies/<slug:number>/addresses',
        AddressListAPIView.as_view(),
        name='api-company_addresses'
        ),
    path('api/token', ObtainExpiringAuthToken.as_view(), name='api-get_token'),
])

urlpatterns.extend(api_viewsets)
urlpatterns.extend(api_urlpatterns)
