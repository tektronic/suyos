"""Views used on the accounts app."""
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse_lazy
from django.utils import translation, timezone

from core.views import get_next_url

from .models import Profile, Company, Address
from .forms import CompanyForm, AddressForm

from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListCreateAPIView
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.response import Response

from .serializers import CompanySerializer, AddressSerializer


class ObtainExpiringAuthToken(ObtainAuthToken):
    """Gets an expiring token for rest_framework."""

    def post(self, request, *args, **kwargs):
        """On post, obtain an expiring token."""
        serializer = self.serializer_class(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        if not created:
            token.created = timezone.now()
            token.save()
        return Response({'token': token.key})


class CompanyViewSet(ReadOnlyModelViewSet):
    """Viewset to provide an API for the Companies."""

    permission_classes = (IsAuthenticated,)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    lookup_field = 'number'


class AddressListAPIView(ListCreateAPIView):
    """List Addresses api."""

    serializer_class = AddressSerializer

    def get_queryset(self, *args, **kwargs):
        """Queryset the addresses for either a company or a profile."""
        company = Company.objects.get(number=self.kwargs.get('number'))
        return company.addresses.all()

    def get(self, request, *args, **kwargs):
        """Get a list of addresses."""
        return self.list(request, *args, **kwargs)


class _LoginView(LoginView):
    """Login View overload."""

    def get_success_url(self):
        """Set the correct language based on user preferences."""
        url = super().get_success_url()
        user = self.request.user
        # maybe check if login was successful?
        translation.activate(user.language)
        self.request.session[translation.LANGUAGE_SESSION_KEY] = user.language
        return url


class _LogoutView(LogoutView):
    """Logout view overload."""

    def get_next_page(self):
        """Restore language to application default.

        This may be deprecated, investigate if this is still necesary.
        """
        next_page = super().get_next_page()
        translation.deactivate()
        self.request.session[translation.LANGUAGE_SESSION_KEY] = \
            translation.get_language()
        # settings.LANGUAGE_CODE?
        return next_page


class CompanyListView(PermissionRequiredMixin, ListView):
    """Logic to show a list of a company."""

    model = Company
    context_object_name = 'company_list'
    permission_required = ('accounts.can_see_company_link')
    title = None

    def get_context_data(self, *args, **kwargs):
        """Set context expected for the template."""
        translation.activate(self.request.user.language)
        context = super().get_context_data(*args, **kwargs)
        context['company_link_class'] = 'active'
        if self.title:
            context['title'] = self.title
        return context


class CompanyCreateView(PermissionRequiredMixin, CreateView):
    """Logic to create a company."""

    permission_required = (
        'accounts.can_see_company_link',
        'accounts.add_company',
    )
    form_class = CompanyForm
    template_name = 'accounts/company_form.html'
    context_object_name = 'company'
    title = None

    def get_success_url(self):
        """Get correct redirect after creation of a Company."""
        next_url, kwargs = get_next_url(self.request.GET.get('next', None))
        if not next_url:
            next_url = 'accounts:companies'
        return reverse_lazy(next_url, kwargs=kwargs)

    def get_form_kwargs(self):
        """Get extra form arguments required by Company Form."""
        kwargs = super().get_form_kwargs()
        kwargs['modified_by'] = self.request.user
        return kwargs

    def get_context_data(self, *args, **kwargs):
        """Set context expected by the template."""
        context = super().get_context_data(*args, **kwargs)
        context['company_link_class'] = 'active'
        if self.title:
            context['title'] = self.title
        return context


class CompanyUpdateView(PermissionRequiredMixin, UpdateView):
    """Logic required for updating of a company."""

    permission_required = ('accounts.change_company')
    model = Company
    form_class = CompanyForm
    template_name = 'accounts/company_form.html'
    context_object_name = "company"
    title = None
    slug_field = 'number'
    slug_url_kwarg = 'number'

    def get_success_url(self):
        """Get correct redirection."""
        next_url, kwargs = get_next_url(self.request.GET.get('next', None))
        if not next_url:
            next_url = 'accounts:companies'
        return reverse_lazy(next_url, kwargs=kwargs)

    def get_form_kwargs(self):
        """Get extra arguments required by the form."""
        kwargs = super().get_form_kwargs()
        kwargs['modified_by'] = self.request.user
        return kwargs

    def get_context_data(self, *args, **kwargs):
        """Get context data required by template."""
        context = super().get_context_data(*args, **kwargs)
        context['company_link_class'] = 'active'
        context['addresses'] = self.object.addresses.all()
        if self.title:
            context['title'] = self.title
        return context


class AddressListView(PermissionRequiredMixin, ListView):
    """Display a list of addresses for Companies or Profiles.

    TODO: implement the functionality for Profiles
    """

    model = Address
    context_object_name = 'address_list'
    permission_required = ('accounts.can_see_company_link')
    title = None

    def get_queryset(self):
        """Get the addresses either from a company or a profile."""
        number = self.kwargs.get('number')
        username = self.kwargs.get('username')
        if number:
            company = Company.objects.get(number=number)
        if username:
            profile = Profile.objects.get(username=username)

        return company.addresses.all() if company else profile.addresses.all()

    def get_context_data(self, *args, **kwargs):
        """Get extra context data required by template."""
        context = super().get_context_data(*args, **kwargs)
        context['number'] = self.kwargs.get('number')
        if self.title:
            context['title'] = self.title
        return context


class AddressCreateView(PermissionRequiredMixin, CreateView):
    """Logic for creating an Address."""

    permission_required = ('accounts.add_address')
    form_class = AddressForm
    template_name = 'accounts/address_form.html'
    title = None
    company = None
    profile = None

    def get_success_url(self):
        """Get redirect url after successful creation of Address."""
        next_url, kwargs = get_next_url(self.request.GET.get('next', None))
        if not next_url:
            next_url = 'accounts:company_addresses'
            kwargs['number'] = self.kwargs.get('number')
        return reverse_lazy(next_url, kwargs=kwargs)

    def get_form_kwargs(self):
        """Get extra arguments required by the form."""
        kwargs = super().get_form_kwargs()
        kwargs['modified_by'] = self.request.user
        kwargs['number'] = self.kwargs.get('number')
        return kwargs


class AddressUpdateView(PermissionRequiredMixin, UpdateView):
    """Logic required to update an Address."""

    model = Address
    form_class = AddressForm
    template_name = 'accounts/address_form.html'
    title = None
    permission_required = ('accounts.add_address')

    def get_success_url(self):
        """Get correct redirection url after update of Address.

        TODO: use for profiles as well
        """
        next_url, kwargs = get_next_url(self.request.GET.get('next', None))
        if not next_url:
            next_url = 'accounts:company_addresses'
            kwargs['number'] = self.kwargs.get('number')
        return reverse_lazy(next_url, kwargs=kwargs)

    def get_form_kwargs(self):
        """Get extra arguments required by the form."""
        kwargs = super().get_form_kwargs()
        kwargs['modified_by'] = self.request.user
        kwargs['number'] = self.kwargs.get('number')
        return kwargs
