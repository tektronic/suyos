from django.apps import apps as django_apps
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _

PRINTER = 'printer'
MONITOR = 'monitor'
PROJECTOR = 'projector'
KEYBOARD = 'keyboard'
MOUSE = 'mouse'
LAPTOP = 'laptop'
DESKTOP = 'desktop'
PROCESSOR = 'processor'
MEMORY = 'memory'
HDD = 'hdd'
MAINBOARD = 'mainboard'
CDD = 'cdd'
CASE = 'case'
TONER = 'toner'
UNKNOWN = 'unknown'#used when a product in warehouse is not 'completed' created
PRODUCTS = {
    PRINTER     : _('Printer'),
    MONITOR     : _('Monitor'),
    PROJECTOR   : _('Projector'),
    KEYBOARD    : _('Keyboard'),
    MOUSE       : _('Mouse'),
    LAPTOP      : _('Laptop'),
    DESKTOP     : _('Desktop'),
    PROCESSOR   : _('Processor'),
    MEMORY      : _('Memory'),
    HDD         : _('Hard Disk'),
    MAINBOARD   : _('Mainboard'),
    CDD         : _('CD Drive'),
    CASE        : _('Case'),
    TONER       : _('Toner'),
    UNKNOWN     : _('Unknown')
}

RENTABLE_PRODUCTS = [
    PRINTER,
    MONITOR,
    PROJECTOR,
    KEYBOARD,
    MOUSE,
    LAPTOP,
    DESKTOP,
]

def get_model(model):
    ''' Gets a model. wrapper over apps.get_model()
    Params:
        model: a string of the format <app_label>.<model>.
    Returns:
        a model instance.
    Raises:
        ImproperlyConfigured if model is of incorrect format or model is not
        in the installed apps.
    '''
    try:
        return django_apps.get_model(model)
    except ValueError:
        raise ImproperlyConfigured(
            "{} must be of the format 'app_label.model_name'".format(model)
        )
    except LookupError:
        raise ImproperlyConfigured(
            "'{}' model has not been installed".format(model)
        )
