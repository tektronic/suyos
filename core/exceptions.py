
class MissingProfileException(Exception):
    ''' Exception to be used when an action requires a Profile.
    '''
    def __init__(self, message, **kwargs):
        '''
        Setting up custom init actions.
        '''
        self.message = message
        super(MissingProfileException, self).__init__(message, **kwargs)


class MissingUpdateProfileError(MissingProfileException):
    ''' raised when update profile is missing. '''
    message = 'Missing Profile for update action.'

    def __init__(self, message=None, **kwargs):
        self.message = message if message else self.message
        super(MissingUpdateProfileError, self).__init__(self.message)


class MissingCreateProfileError(MissingProfileException):
    ''' raised when create profile is missing. '''
    message = 'Missing Profile for create action.'

    def __init__(self, message=None, **kwargs):
        self.message = message if message else self.message
        super(MissingCreateProfileError, self).__init__(self.message)
