from django import forms


class AbstractModelForm(forms.ModelForm):
    ''' Represent an abstract model form.
    When a model form is abstract, we dont really have a model for it.
    '''

    def __init__(self, *args, **kwargs):
        ''' get the model from one of the kwargs '''
        model = kwargs.pop('model', None)
        self._meta.model = model if not self._meta.model else self._meta.model
        super(AbstractModelForm, self).__init__(*args, **kwargs)


class AuditableFormMixin(forms.ModelForm):
    ''' in charge of getting a profile for the form. '''

    def __init__(self, *args, **kwargs):
        ''' Get a profile that is modifying/creating the form '''
        self.profile = kwargs.pop('modified_by', None)
        super().__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        ''' '''
        self.instance.modified_by = self.profile
        super().save(*args, **kwargs)
