��    &      L  5   |      P  !   Q     s     �  D   �  	   �  
   �     �     �  	   �  =   �     8     A     G  	   N     X     _  !   g     �     �     �  	   �     �     �  	   �  	   �     �  	          E        ^      m     �     �  	   �  2   �     �     �  k    #   w     �     �  T   �     
               %  
   +  I   6     �     �     �     �     �     �  #   �     �     	  	   	  
   	  	   '	  !   1	  	   S	     ]	  
   e	     p	     w	  P   	     �	  6   �	     
     *
     6
  ;   ?
     {
     �
                    	             
                               "                              %             &                 $   !                                       #            Able to place orders to providers Add Products Bills Buy our Products. We have a great selection of new and used products Companies Currencies Currency Dollar Hard Disk Have a client but don't have enough products. Partner with us Keyboard Login Logout Mainboard Memory Monitor Must provide a exchange currency. New Products in Stock Notifications Printer Processor Products Products need to be picked up Projector Purchases Rentals Sales Tax Sales Taxes Sending email to renters and buyers when the order is being procesed. System Updates The place for all your IT needs. This is hardcoded Unknown Warehouse We have lots of items in stock that can be rented. Welcome %(name)s Welcome to Tektronic Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-13 13:36-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Capacidad de ordenar de proveedores Agregar Producto Facturas Compra nuestors productos. Tenemos una gran selección de productos nuevoes y usados Compañías Monedas Moneda Dolar Disco Duro Tienes clientes, pero no tienes suficientes productos?. Dejanos ayudarte. Teclado Iniciar Session Cerrar Session Tarjeta Madre Memoria Monitor Debes proveer una moneda de cambio. Nuevos productos en stock Notificaciones Impresora Procesador Productos Productos necesitan ser recogidos Proyector Compras Alquileres I.G.V. I.G.V.s Enviar correo a alquiladores y compradores cuando la orden esta siendo procesada Actualizar systemas El lugar ideal para todos tus necesidades de systemas. Dato hardcoded Desconosido Almacén Tenemos muchos productos en stock que pueden ser alquilados Bienvenido %(name)s Bienvenido a Tektronic 