from django.core.management.base import BaseCommand

from core.models import Currency

def create_currencies():
    '''
    '''
    dollar = Currency.objects.get_or_create(
        force_create=True,
        code=Currency.DOLLAR,
        symbol=Currency.SYMBOLS.get(Currency.DOLLAR, None),
    )
    for c in Currency.CURRENCIES:
        if c is not Currency.DOLLAR:
            Currency.objects.get_or_create(
                force_create=True,
                symbol= Currency.SYMBOLS.get(c, None),
                code=c,
                exchange_rate=3.27,
                exchange_currency=dollar
            )

class Command(BaseCommand):
    '''
    '''
    help = 'Initialize some data in core. Depends on custom_auth commands'

    def handle(self, *args, **options):
        create_currencies()
