"""Common functionality for models."""
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.forms.models import model_to_dict
from django.utils.translation import gettext as _

# from simple_history.models import HistoricalRecords

from .exceptions import (
    MissingProfileException,
    MissingCreateProfileError,
    MissingUpdateProfileError
)


class AbstractProduct(models.Model):
    """All products must have this behaviour."""

    serial_number = models.CharField(max_length=50, unique=True)

    class Meta:
        """AbstractProduct meta."""

        abstract = True


class ModelDiffMixin:
    """A model mixin that tracks model fields' values.

    It also provides a useful api to know what fields have been changed.
    """

    def __init__(self, *args, **kwargs):
        """Set __initial dictionary."""
        super().__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def diff(self):
        """Check differences so far."""
        d1 = self.__initial
        d2 = self._dict
        diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k]]
        return dict(diffs)

    @property
    def has_changed(self):
        """Check if something has changed."""
        return bool(self.diff)

    @property
    def changed_fields(self):
        """Check which properties have changed."""
        return self.diff.keys()

    def get_field_diff(self, field_name):
        """Return a diff for field if it's changed and None otherwise."""
        return self.diff.get(field_name, None)

    def save(self, *args, **kwargs):
        """Save model and set initial state."""
        super(ModelDiffMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        return model_to_dict(
            self,
            fields=[field.name for field in self._meta.fields]
        )


class AuditableQuerySet(models.QuerySet):
    """Options for queries on an auditable."""

    def update(self, *args, **kwargs):
        """Check for modified_by in the query."""
        modified_by = kwargs.get('modified_by', None)
        if not modified_by:
            raise MissingUpdateProfileError()
        return super(AuditableQuerySet, self).update(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """Do not delete?."""
        super().delete(*args, **kwargs)


class AuditableManager(models.Manager):
    """Manager for all auditable models."""

    def get_queryset(self):
        """Get the queryset for auditables."""
        return AuditableQuerySet(self.model, using=self._db)

    def create(self, *args, **kwargs):
        """Create an auditable model.

        Returns:
            A newly created model.

        """
        kwargs = self.clean(*args, **kwargs)
        return super(AuditableManager, self).create(*args, **kwargs)

    def clean(self, *args, **kwargs):
        """Make sure that the kwargs given are proper for saving an auditable.

        created_by, modified_by and force_create can be passed in the kwargs.
        if force_create flag is passed, the method will try to create an
        auditable model with a superuser on the system.

        Raises:
            MissingProfileException:
                * If modified_by and created_by and force_create are not
                  provided.
                * if modified_by and created_by are not provided and
                  force create is provided, but there is no superusers in
                  they system

        Returns:
            A proper version of kwargs.

        """
        modified_by = kwargs.get('modified_by', None)
        created_by = kwargs.pop('created_by', None)
        force_create = kwargs.pop('force_create', None)
        if not modified_by and not created_by:
            # TODO: Identify if force create is a good idea.
            if force_create:
                user_model = get_user_model()
                created_by = user_model.objects.filter(
                    is_superuser=True
                ).first()
                if not created_by:
                    raise MissingCreateProfileError(
                        message='No Superuser available for create action.'
                    )
            else:
                raise MissingCreateProfileError()
        kwargs['modified_by'] = created_by if created_by else modified_by
        return kwargs


class Auditable(models.Model):
    """Model an entity that can be auditable.

    When updatig an auditable class, make sure you pass a
    """

    modified_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.DO_NOTHING,
        related_name='+'  # dont need reference to who changed the record.
    )
    modified = models.DateTimeField(auto_now=True)
    # comment when running tests...
    # or make ModelMixinTestCase compatible with
    # historical records (Auditable)
    # history = HistoricalRecords(inherit=True)

    objects = AuditableManager()

    MissingProfileException = MissingProfileException

    @property
    def _history_user(self):
        return self.modified_by

    def save(self, *args, **kwargs):
        """Save an auditable model.

        TODO:
        * revise if this approach is good since 'kwargs' are not passed
          from the forms hence we never get the modified by and such.
          Also, if you try to use 'ModelDiffMixin' beware that if the
          same user modifies the model, it won't count as modified...
        * update tests
        """
        modified_by = kwargs.pop('modified_by', None)
        created_by = kwargs.pop('created_by', None)
        if self.id:
            # Update
            if not self.modified_by:
                raise MissingUpdateProfileError(
                    _("Model's modified_by has not been updated.")
                )
            if modified_by:
                self.modified_by = modified_by
        else:
            # Create
            # self.modified_by will be set by the auditable manager's create
            # function. In which case, it will not come in the keyword
            # arguments. this also considers the case where save gets called
            # without the use of manager, in which case, keyword arguments
            # must be provided.
            if not self.modified_by_id and not created_by and not modified_by:
                raise MissingCreateProfileError()
            elif not self.modified_by_id:
                self.modified_by = created_by if created_by else modified_by
        return super(Auditable, self).save(*args, **kwargs)

    class Meta:
        """ClassMeta for Auditable."""

        abstract = True


class SalesTax(Auditable):
    """SalesTaxes applied to apps.

    TODO: write a clean method and format percentage accordingly.
    """

    percentage = models.DecimalField(
        max_digits=5,
        decimal_places=4
    )
    valid_from = models.DateField()
    valid_to = models.DateField()

    def __str__(self):
        """Integer representation of a percentage."""
        return '{}%'.format(int(self.percentage*100))

    class Meta:
        """SalesTax meta"."""

        verbose_name = _('Sales Tax')
        verbose_name_plural = _("Sales Taxes")


class CurrencyManager(AuditableManager):
    """Currency manager."""

    def get_or_create(self, *args, **kwargs):
        """Get a Currency or tries to create one."""
        try:
            c = Currency.objects.get(code=kwargs.get('code'))
        except Currency.DoesNotExist:
            c = super(CurrencyManager, self).create(*args, **kwargs)
        return c


class Currency(Auditable):
    """Currency used in the application."""

    SOL = 'pen'
    DOLLAR = 'usd'
    EURO = 'eur'
    CURRENCIES = {
        SOL: _('Nuevo Sol'),
        DOLLAR: _('Dollar'),
        EURO: _('Euro'),
    }
    SYMBOLS = {
        SOL: 'S/.',
        DOLLAR: '$',
        EURO: '€'
    }
    code = models.CharField(
        choices=sorted(CURRENCIES.items()),
        max_length=3,
        unique=True,
    )
    symbol = models.CharField(
        choices=sorted(SYMBOLS.items()),
        max_length=3,
        unique=True,
        null=True,
        default=None
    )
    exchange_rate = models.DecimalField(
        max_digits=9,
        decimal_places=4,
        null=True,
        default=None,
    )
    exchange_currency = models.ForeignKey(
        settings.CURRENCY_MODEL,
        on_delete=models.DO_NOTHING,
        null=True,
        default=None,
        # TODO change the name? this foreign key maps to the currency that the
        # exchange rate relates to. For example:
        #   code: pen;
        #   symbol: S./;
        #   exchange_rate: 3.51;
        #   exchange_currency: <Dollar currency>
        related_name='related_currency'
    )

    objects = CurrencyManager()

    def __str__(self):
        """Get string representation of Currencies."""
        return self.CURRENCIES[self.code]

    def clean(self):
        """If you have an exchange rate, you must have a currency_id."""
        super(Currency, self).clean()
        if self.exchange_rate and not self.exchange_currency_id:
            raise ValidationError(_('Must provide a exchange currency.'))

    class Meta:
        """Meta class for Currency."""

        verbose_name = _('Currency')
        verbose_name_plural = _("Currencies")
