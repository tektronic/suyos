from django import template
from django.template.defaultfilters import stringfilter

from core.models import Currency

register = template.Library()

@register.filter
@stringfilter
def get_currency_display(value):
    ''' '''
    return Currency.CURRENCIES.get(value, None)
