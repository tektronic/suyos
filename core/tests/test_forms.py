from django.contrib.auth import get_user_model
from django.db import connection
from django.db.models.base import ModelBase
from django.test import TestCase

from .test_models import ModelMixinTestCase
from ..models import Auditable
from ..forms import *


class FormMixinTestCase(TestCase):
    ''' Direct copy from ModelMixinTestCase
    TODO: investigate why cant you use this from the model itself....
    '''
    @classmethod
    def setUpClass(cls):
        # Create a dummy model which extends the mixin
        cls.model = ModelBase('__TestForm__' +
            cls.mixin.__name__, (cls.mixin,),
            {'__module__': cls.mixin.__module__})

        # Create the schema for  our test model
        with connection.schema_editor() as schema_editor:
            schema_editor.create_model(cls.model)
        super(FormMixinTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        # Delete the schema for the test model
        with connection.schema_editor() as schema_editor:
            schema_editor.delete_model(cls.model)
        super(FormMixinTestCase, cls).tearDownClass()
"""
class AbstractModelFormTest(TestCase):
    ''' test an abstract model form '''

    def test_init_gets_model(self):
        ''' gets the model before calling its initialization.
        Mostly used for testing purposes. most forms must have a model.
        '''
        with self.assertRaises(ValueError):
            AbstractModelForm()
        try:
            form = AbstractModelForm(model=get_user_model)
        except Exception:
            self.fail('instantiation of form raises an unexpected error')

class AuditableFormMixinTestCase(FormMixinTestCase):
    mixin = Auditable

    def setUp(self):
        ''' '''
        self.test_user = get_user_model().objects.create(username='test_user')

    def test_clean(self):
        ''' test the clean functionality of an Auditable form '''
        f = AuditableFormMixin(model=self.model)
        self.assertFalse(f.is_valid())
        f = AuditableFormMixin(model=self.model, created_by=self.test_user)
        self.assertTrue(f.is_valid())
        f = AuditableFormMixin(model=self.model, modified_by=self.test_user)
        self.assertTrue(f.is_valid())
"""
