"""Models tests.

Contains test for the core app.
TODO: test verbose_name and verbose_name_plural
"""
from datetime import timedelta

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import connection, IntegrityError
from django.db.models.base import ModelBase
from django.test import TestCase
from django.utils import timezone

from ..models import (
    AbstractProduct,
    Auditable,
    Currency,
    SalesTax
    )
from ..exceptions import MissingProfileException


class ModelMixinTestCase(TestCase):
    """Base class for tests of model mixins.

    To use, subclass and specify the mixin class variable. A model using
    the mixin will be made available in self.model
    """

    @classmethod
    def setUpClass(cls):
        """Create a dummy model which extends the mixin."""
        cls.model = ModelBase(
            '__TestModel__' + cls.mixin.__name__,
            (cls.mixin,),
            {'__module__': cls.mixin.__module__}
            )

        # Create the schema for  our test model
        with connection.schema_editor() as schema_editor:
            schema_editor.create_model(cls.model)
        super(ModelMixinTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """Delete the schema for the test model."""
        with connection.schema_editor() as schema_editor:
            schema_editor.delete_model(cls.model)
        super(ModelMixinTestCase, cls).tearDownClass()


class AbstractProductTestCase(ModelMixinTestCase):
    """Test for an abstract product."""

    mixin = AbstractProduct

    def test_serial_number_label(self):
        """Test serial number label for Abstract Products."""
        ap = self.model.objects.create(serial_number='1')
        label = ap._meta.get_field('serial_number').verbose_name
        self.assertEqual(label, 'serial number')

    def test_serial_number_max_length(self):
        """Test max number of serial numbers."""
        ap = self.model.objects.create(serial_number='2')
        max_length = ap._meta.get_field('serial_number').max_length
        self.assertEqual(max_length, 50)

    def test_serial_number_unique(self):
        """Test that serial numbers are unique."""
        self.model.objects.create(serial_number='1')
        with self.assertRaises(IntegrityError):
            self.model.objects.create(serial_number='1')


class AuditableTestCase(ModelMixinTestCase):
    """Tests Abstract auditable class."""

    mixin = Auditable
    auth_model = get_user_model()

    def setUp(self):
        """Set initial data for Auditable TestCase."""
        self.create_user = self.auth_model.objects.create_superuser(
            "cuser",
            "password"
            )
        self.auth_model.objects.create_superuser(
            "muser",
            "password"
            )
        self.model.objects.create(created_by=self.create_user)

    def test_modified_by_label(self):
        """Test modified by label."""
        audit = self.model.objects.get(pk=1)
        field_label = audit._meta.get_field('modified_by').verbose_name
        self.assertEqual(field_label, 'modified by')

    def test_modified_label(self):
        """Test modified label."""
        audit = self.model.objects.get(pk=1)
        field_label = audit._meta.get_field('modified').verbose_name
        self.assertEqual(field_label, 'modified')

    def test_create_without_profile(self):
        """Test create without a profile."""
        with self.assertRaises(MissingProfileException):
            self.model.objects.create()
        _ = self.model()
        with self.assertRaises(MissingProfileException):
            _.save()

    def test_create_with_profile(self):
        """Test creation of an auditable model with a profile."""
        create_user = self.auth_model.objects.get(username='cuser')
        modify_user = self.auth_model.objects.get(username='muser')
        # create with create user
        audit = self.model.objects.create(created_by=create_user)
        self.assertEqual(create_user, audit.modified_by)
        # create with modify user
        audit = self.model.objects.create(modified_by=modify_user)
        self.assertEqual(modify_user, audit.modified_by)
        # create with create and modify user
        audit = self.model.objects.create(
            created_by=create_user, modified_by=modify_user
        )
        self.assertEqual(create_user, audit.modified_by)

    def test_force_create_with_su(self):
        """Force create with superuser."""
        # su = self.auth_model.objects.create_superuser('su', 'password')
        audit = self.model.objects.create(force_create=True)
        # because self.create_user is the first super user created.
        self.assertEqual(audit.modified_by, self.create_user)

    def test_create_with_profile_without_manager(self):
        """Test the save method..."""
        create_user = self.auth_model.objects.get(username='cuser')
        modify_user = self.auth_model.objects.get(username='muser')
        audit = self.model()
        audit.save(created_by=create_user)
        self.assertEqual(create_user, audit.modified_by)
        audit = self.model()
        audit.save(modified_by=modify_user)
        self.assertEqual(modify_user, audit.modified_by)
        audit = self.model()
        audit.save(modified_by=modify_user, created_by=create_user)
        self.assertEqual(create_user, audit.modified_by)

    def test_modify_without_profile(self):
        """Test modify without profile."""
        self.auth_model.objects.get(username='cuser')
        audit = self.model.objects.get(pk=1)
        audit.modified = timezone.now()

        # TODO: Update test when proper solution is found for auditables...
        # with self.assertRaises(MissingProfileException):
        #     audit.save()

    def test_modify_with_profile(self):
        """Test modify with profile."""
        # self.auth_model.objects.get(username='cuser')
        modify_user = self.auth_model.objects.get(username='muser')
        audit = self.model.objects.get(pk=1)
        audit.modified = timezone.now() + timedelta(days=1)
        audit.save(modified_by=modify_user)
        self.assertEqual(modify_user, audit.modified_by)
        # test if the date gets saved?
        audit.modified = timezone.now() + timedelta(days=2)

        # TODO: Update test when proper solution is found for auditables...
        # with self.assertRaises(MissingProfileException):
        #     audit.save(created_by=modify_user)

    def test_multiple_update(self):
        """Testing updates on Querysets."""
        create_user = self.auth_model.objects.get(username='cuser')
        modify_user = self.auth_model.objects.get(username='muser')
        for i in range(3):
            self.model.objects.create(created_by=create_user)
        qs = self.model.objects.all()
        with self.assertRaises(MissingProfileException):
            qs.update(modified=timezone.now())
        try:
            qs.update(modified=timezone.now(), modified_by=modify_user)
        except Exception:
            self.fail("queryset update raised an unexpected exception")


class CurrencyTestCase(TestCase):
    """Tests for the currency models.

    TODO: move this to the appropriate module if currency model gets moved.
    """

    auth_model = get_user_model()

    def setUp(self):
        """Set test data for currency TestCase."""
        self.auth_model.objects.create_superuser(
            'cuser',
            'password'
            )
        self.auth_model.objects.create_superuser('suser', 'password')
        Currency.objects.create(
            code=Currency.DOLLAR, force_create=True
        )

    def test_currency_labels(self):
        """Test Currency labels.

        # TODO: make sure that choices on some of the fields are using the
        # correct dictionary...
        """
        cur = Currency.objects.get(pk=1)

        code_field = cur._meta.get_field('code')
        label = code_field.verbose_name
        # options = code_field.choices
        max_length = code_field.max_length
        unique = code_field.unique
        self.assertEqual(label, 'code')
        # self.assertTrue(options == Currency.CURRENCIES.items())
        self.assertEqual(max_length, 3)
        self.assertEqual(unique, True)

        symbol_field = cur._meta.get_field('symbol')
        label = symbol_field.verbose_name
        # options = symbol_field.choices
        max_length = symbol_field.max_length
        unique = symbol_field.unique
        null = symbol_field.null
        default = symbol_field.default
        self.assertEqual(label, 'symbol')
        # self.assertEqual(options, list(Currency.SYMBOLS.items()))
        self.assertEqual(max_length, 3)
        self.assertEqual(unique, True)
        self.assertEqual(null, True)
        self.assertEqual(default, None)

        erf = cur._meta.get_field('exchange_rate')
        max_digits = erf.max_digits
        decimal_places = erf.decimal_places
        label = erf.verbose_name
        null = erf.null
        default = erf.default
        self.assertEqual(label, 'exchange rate')
        self.assertEqual(null, True)
        self.assertEqual(default, None)
        self.assertEqual(max_digits, 9)
        self.assertEqual(decimal_places, 4)

        ecf = cur._meta.get_field('exchange_currency')
        label = ecf.verbose_name
        null = ecf.null
        default = ecf.default
        # related_name = ecf.related_name
        self.assertEqual(label, 'exchange currency')
        self.assertEqual(null, True)
        self.assertEqual(default, None)
        # self.assertEqual(related_name,'related_currency')

    def test_clean(self):
        """Test currency clean."""
        cur = Currency.objects.get(pk=1)
        create_user = self.auth_model.objects.get(pk=1)
        _ = Currency(modified_by=create_user, exchange_rate=2.31)
        with self.assertRaises(ValidationError):
            _.clean()
        try:
            cur.clean()
        except Exception:
            self.fail("Currency.clean() raised an unexpected exception")

    def test_get_or_create(self):
        """Test get_or_create from model."""
        dollar = Currency.objects.get(code=Currency.DOLLAR)
        cur = Currency.objects.get_or_create(code=Currency.DOLLAR)
        self.assertEqual(cur, dollar)

        with self.assertRaises(MissingProfileException):
            Currency.objects.get_or_create(code=Currency.EURO)
        try:
            Currency.objects.get_or_create(
                code=Currency.EURO,
                force_create=True
            )
        except Exception:
            self.fail('Currency.objects.get_or_create() raised an exception')


class SalesTaxTestCase(TestCase):
    """SalesTax TestCase."""

    auth_model = get_user_model()

    def setUp(self):
        """Set data for SalesTax TestCase."""
        create_user = self.auth_model.objects.create_superuser(
            'cuser',
            'password'
            )
        SalesTax.objects.create(
            created_by=create_user,
            percentage=0.21,
            valid_from=timezone.now(),
            valid_to=timezone.now() + timedelta(days=60)
        )

    def test_sales_tax_labels(self):
        """Test SalesTax labels."""
        tax = SalesTax.objects.get(pk=1)
        pf = tax._meta.get_field('percentage')
        label = pf.verbose_name
        max_digits = pf.max_digits
        decimals = pf.decimal_places
        self.assertEqual(label, 'percentage')
        self.assertEqual(max_digits, 5)
        self.assertEqual(decimals, 4)

        label = tax._meta.get_field('valid_from').verbose_name
        self.assertEqual(label, 'valid from')
        label = tax._meta.get_field('valid_to').verbose_name
        self.assertEqual(label, 'valid to')

    def test_str(self):
        """Test string representation of SalesTax."""
        tax = SalesTax.objects.get(pk=1)
        tax.percentage = 0.1111
        self.assertEqual(str(tax), '11%')
