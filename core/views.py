from django.views.generic import TemplateView

def get_next_url(next_string, separator = '-'):
    ''' parses the next_string to look for a url and its keyword arguments
     the url must be of the format:
     <url>[<separator><key>=<value>,<key>=<value>...]
    '''
    kwargs = {}
    next_url = next_string.split(separator) if next_string else None
    url = next_url[0] if next_url else None
    raw_kwargs = next_url[-1] if next_url and len(next_url) > 0 else None
    if raw_kwargs:
        _kwargs = {}
        for kwarg in raw_kwargs.split(','):
            if kwarg:
                lst = kwarg.split('=')
                if len(lst) == 2:
                    _kwargs[lst[0]] = lst[1]
        kwargs = _kwargs
    return (url, kwargs)

class Homepage(TemplateView):
    template_name = 'core/index.html'
    extra_context = None

    def get_context_data(self, **kwargs):
        context = super(Homepage, self).get_context_data(**kwargs)
        if self.extra_context:
            context.update(extra_context)
        return context
