"""Contains constants used in purchases."""
from django.utils.translation import ugettext_lazy as _

LEGACY = 'legacy'
ORIGIN = 'origin'
COMPLEX = 'complex'

LOADING = 'loading'
COMPLETED = 'completed'
DELETED = 'deleted'
STATUSES = {
    LOADING: _('Loading'),
    COMPLETED: _('Completed'),
    DELETED: _('Deleted'),
}
