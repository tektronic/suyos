"""Configuration for app."""
from django.apps import AppConfig


class PurchasesConfig(AppConfig):
    """Comment."""

    name = 'purchases'
