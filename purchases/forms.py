"""Handle forms used in purchases."""
from decimal import Decimal

from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.utils.translation import gettext_lazy as _

# avoid importing from other modules other than core!
from accounts.models import Company
from core.forms import AuditableFormMixin

from . import COMPLETED
from .models import Bill, Receipt, Product


class SeparatedValuesField(forms.Field):
    """Custom Separated Values field.

    A Django newforms field which takes another newforms field during
    initialization and validates every item in a separated list with
    this field class. Please use it like this:

        from django.newforms import EmailField
        emails = SeparatedValuesField(EmailField)

    You would be able to enter a string like "john@doe.com,guido@python.org"
    because every email address would be validated when clean() is executed.
    This of course also applies to any other Field class.

    You can define the sepator (default: ",") during initialization with the
    ``separator`` parameter like this::

        from django.newforms import EmailField
        emails = SeparatedValuesField(EmailField, separator="###")

    If validation succeeds it returns the original data, though the already
    splitted value list can be accessed with the get_list() method.

    >>> f = SeparatedValuesField(forms.EmailField)
    >>> f.clean("foo@bar.com,bar@foo.com")
    'foo@bar.com,bar@foo.com'
    >>> f.get_list()
    ['foo@bar.com', 'bar@foo.com']
    >>> f.clean("foobar,foo@bar.com,bar@foo.com")
    Traceback (most recent call last):
        ...
    ValidationError: <unprintable ValidationError object>
    >>> u = SeparatedValuesField(forms.URLField)
    >>> u.clean("http://foo.bar.com,http://foobar.com")
    'http://foo.bar.com,http://foobar.com'
    >>> u.clean("http:foo.bar.com")
    Traceback (most recent call last):
        ...
    ValidationError: <unprintable ValidationError object>
    >>> f = SeparatedValuesField(forms.EmailField, separator="###")
    >>> f.clean("foo@bar.com###bar@foo.com")
    'foo@bar.com###bar@foo.com'
    >>> f.clean("foobar###foo@bar.com###bar@foo.com")
    Traceback (most recent call last):
        ...
    ValidationError: <unprintable ValidationError object>

    """

    def __init__(
        self,
        base_field=None,
        separator=",",
        widget=forms.Textarea,
        *args,
        **kwargs
    ):
        """Set up default fields."""
        super(SeparatedValuesField, self).__init__(*args, **kwargs)
        self.base_field = base_field
        self.separator = separator
        self.widget = widget()

    def clean(self, data):
        """Split the results added to the value_list."""
        if not data:
            if self.required:
                raise forms.ValidationError('Enter at least one value.')
            else:
                return None
        self.value_list = [
            d.strip().upper() for d in data.split(self.separator)
        ]
        if self.base_field is not None:
            base_field = self.base_field()
            for value in self.value_list:
                base_field.clean(value)
        return self.value_list


class BillForm(AuditableFormMixin):
    """Represents a bill in the system."""

    receipts = SeparatedValuesField(base_field=forms.CharField)

    def __init__(self, *args, **kwargs):
        """Set up initial values."""
        self.bill = kwargs.pop('bill', None)
        super().__init__(*args, **kwargs)
        company_field = self.fields.get('company')
        reciepts_field = self.fields.get('receipts')
        company_field.queryset = Company.objects.filter(
            is_provider=True,
            is_active=True
        )
        receipts = Receipt.objects.filter(bill=self.instance)
        reciepts_field.initial = ', '.join([r.number for r in receipts])

    def clean_number(self, *args, **kwargs):
        """triming the number given."""
        return self.cleaned_data.get('number').replace(' ', '').upper()


    def clean_receipts(self, *args, **kwargs):
        """Handle user input to get receipts.

        Only ensuring that we are only saving new receipts.
        TODO: raise error if you dont have a number...
        """
        receipts = self.cleaned_data.get('receipts')
        existing_receipts = [
            receipt.number for receipt in Receipt.objects.filter(
                bill=self.bill, number__in=receipts
            ) if receipts
        ]
        new_receipts = [
            number for number in receipts if number not in existing_receipts
        ]
        return new_receipts

    def clean_price(self, *args, **kwargs):
        """Check that new price is not lower than the sum of products."""
        new_price = self.cleaned_data.get('price', 0)
        if self.has_changed() and 'price' in self.changed_data and\
           self.instance.products_price > new_price:
            raise ValidationError(_(
                "The sum of the products in the receipts exceeds new price."
                ))
        return new_price

    def save(self, *args, **kwargs):
        """Create receipts models when saving them."""
        _ = super().save(*args, **kwargs)
        receipts = []
        for number in self.cleaned_data.get('receipts', None):
            receipts.append(Receipt(
                number=number.replace(' ', '').upper(),
                bill=self.instance
            ))
        Receipt.objects.bulk_create(receipts)
        return _

    class Meta:
        """Meta class."""

        model = Bill
        exclude = [
            'id',
            'modified_by',
            'status',
        ]


class ProductForm(forms.ModelForm):
    """Updates a product.

    TODO:
    * call a signal to update products in other modules.
    """

    def __init__(self, *args, **kwargs):
        """Get user updating this form."""
        self.profile = kwargs.pop('modified_by', None)
        super().__init__(*args, **kwargs)
        self.instance.previous_serial_number = self.instance.serial_number

    def save(self, *args, **kwargs):
        """Set the model's profile so it can be added to the signal."""
        self.instance.modified_by = self.profile
        super().save(self, *args, **kwargs)

    def clean(self, *args, **kwargs):
        """Ensure that the price does not exeed the price in the bill."""
        if self.has_changed() and 'price' in self.changed_data:
            new_price = self.cleaned_data.get('price', 0)
            new_total = self.instance.receipt.products_price -\
                        self.initial.get('price', 0) + new_price
            if new_total > self.instance.receipt.bill.price:
                raise ValidationError(
                    _("The price of this product exeeds the price of the bill.")
                    )

    class Meta:
        """Product Metaclass."""

        model = Product
        fields = ('serial_number', 'receipt', 'price')


class CreateProductsForm(forms.Form):
    """Create products from this module.

    TODO: should be using a regular form instead of a ModelForm?
    """

    price = forms.DecimalField(
        decimal_places=2,
        max_digits=12,
        validators=[MinValueValidator(Decimal('0.01'))]
    )
    numbers = SeparatedValuesField(base_field=forms.CharField)

    def __init__(self, *args, **kwargs):
        """Set up initial values."""
        self.profile = kwargs.pop('modified_by', None)
        self.receipt = Receipt.objects.get(number=kwargs.pop('receipt_number'))
        kwargs.pop('serial_number')
        # if using a generic update/create, instance is passed causing an
        # exception
        kwargs.pop('instance', None)
        super().__init__(*args, **kwargs)

    def clean_numbers(self, *args, **kwargs):
        """Clean serial numbers.

        Only new serial numbers will be added. If modified/deleted, it won't
        have any effect.
        """
        sns = self.cleaned_data.get('numbers')
        existing_numbers = [
            product.serial_number for product in Product.objects.filter(
                receipt=self.receipt,
                serial_number__in=sns
            )
        ]
        return [number for number in sns if number not in existing_numbers]

    def clean(self, *args, **kwargs):
        """Not really used...

        TODO: add validation when:
        * adding a product to a bill that is inactive
        """
        price = self.cleaned_data.get('price')
        new_items_count = len(self.cleaned_data.get('numbers'))
        total = price * new_items_count
        if total + self.receipt.bill.products_price > self.receipt.bill.price:
            raise ValidationError(_(
                'The bill price is exceeded, Try adding less products, '
                'or update the bill.'
                ))
        return super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        """Override the save since we are saving multiple records here..."""
        _ = None
        bill = self.receipt.bill
        price = self.cleaned_data.get('price')
        ammount_added = 0
        # creating Products one by one, because we need signals to be called...
        for number in self.cleaned_data.get('numbers'):
            Product.objects.create(
                created_by=self.profile,
                serial_number=number,
                receipt=self.receipt,
                price=price
                )
            ammount_added += price
        if bill.price == bill.price + ammount_added:
            bill.status = COMPLETED
            bill.save(modified_by=self.profile)
        # returning the last product added...
        return _
