"""Models used in purchases module."""
from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from core.models import Auditable, AbstractProduct

from . import STATUSES, LOADING, DELETED


class Bill(Auditable):
    """A bill on the system."""

    number = models.CharField(max_length=20, unique=True)
    purchase_date = models.DateField()
    company = models.ForeignKey(
        settings.COMPANY_MODEL,
        on_delete=models.DO_NOTHING,
        related_name="purchase_bills"
    )
    currency = models.ForeignKey(
        settings.CURRENCY_MODEL,
        on_delete=models.DO_NOTHING,
        related_name='+'
    )
    status = models.CharField(
        max_length=10,
        choices=sorted(STATUSES.items()),
        default=LOADING
    )
    price = models.DecimalField(max_digits=12, decimal_places=2)

    @property
    def products_price(self, *args, **kwargs):
        """Get the prices of all the products in all receipts.

        WARNING:
            * O(n^2) => going into receipts and then into products
        """
        return sum([r.products_price for r in self.receipt_set.all()])

    def delete(self, *args, **kwargs):
        """Set the status of the bill to deleted."""
        if kwargs.get('force', None):
            super().delete(*args, **kwargs)
        else:
            self.status = DELETED
            self.save(*args, **kwargs)

    def __str__(self):
        """See number when printing."""
        return str(self.number)

    class Meta:
        """Metadata."""

        verbose_name = _('Bill')
        verbose_name_plural = _('Bills')


class Receipt(models.Model):
    """A receipt on the system."""

    number = models.CharField(max_length=20)
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)

    @property
    def products_price(self, *args, **kwargs):
        """Total price of the products in the receipt."""
        return sum([p.price for p in self.product_set.all()])

    def __str__(self):
        """Show number of a receipt."""
        return self.number

    class Meta:
        """Metaclass."""

        verbose_name = _('Purchase Receipt')
        verbose_name_plural = _('Purchase Receipt')


class ProductManager(models.Manager):
    """Product Manager."""

    def create(self, *args, **kwargs):
        """On creation of a product.

        Need this for the creation of a warehouse product.
        a wharehouse product is an auditable model.
        """
        self.model.created_by = kwargs.pop('created_by', None)
        return super(ProductManager, self).create(*args, **kwargs)


class Product(AbstractProduct):
    """Product information valid for purchases."""

    receipt = models.ForeignKey(Receipt, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=12, decimal_places=2)

    objects = ProductManager()

    def __init__(self, *args, **kwargs):
        """Save the price of this product (for eddits only)."""
        super().__init__(*args, **kwargs)
        self.old_price = self.price

    def __str__(self):
        """Show serial number on display."""
        return self.serial_number

    class Meta:
        """Metaclass."""

        verbose_name = _('Purchased Product')
        verbose_name_plural = _('Purchased Products')
