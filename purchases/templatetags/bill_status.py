"""Tags used in tempaltes."""
from django import template
from django.template.defaultfilters import stringfilter

from purchases import STATUSES

register = template.Library()


@register.filter
@stringfilter
def get_bill_status_display(value):
    """Get the display value of statuses."""
    return STATUSES.get(value, None)
