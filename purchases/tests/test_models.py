"""Tests for the models in the purchses module."""
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.test import TestCase
from django.utils import timezone

from core import get_model

from .. import LOADING, DELETED
from ..models import Bill, Receipt, Product


class BillTestCase(TestCase):
    """Tests bill models."""

    user = None
    company = None
    currency = None
    bill = None

    def setUp(self):
        """Set up data."""
        profile_model = get_model(settings.AUTH_USER_MODEL)
        self.user = profile_model.objects.create(username='cuser')
        company_model = get_model(settings.COMPANY_MODEL)
        self.company = company_model.objects.create(
            name='Company',
            number='1',
            created_by=self.user
        )
        currency_model = get_model(settings.CURRENCY_MODEL)
        self.currency = currency_model.objects.create(
            code='usd',
            created_by=self.user
        )
        self.bill = Bill.objects.create(
            number=1,
            purchase_date=timezone.now(),
            company=self.company,
            currency=self.currency,
            price=0,
            created_by=self.user,
        )

    def test_fields(self):
        """Makes sure the model has the following fields."""
        number_field = self.bill._meta.get_field('number')
        label = number_field.verbose_name
        max_length = number_field.max_length
        unique = number_field.unique
        self.assertEqual(label, 'number')
        self.assertEqual(max_length, 20)
        self.assertEqual(unique, True)

        label = self.bill._meta.get_field('purchase_date').verbose_name
        self.assertEqual(label, 'purchase date')

        label = self.bill._meta.get_field('currency').verbose_name
        self.assertEqual(label, 'currency')

        status_field = self.bill._meta.get_field('status')
        label = status_field.verbose_name
        default = status_field.default
        self.assertEqual(label, 'status')
        self.assertEqual(default, LOADING)

        price_field = self.bill._meta.get_field('price')
        label = price_field.verbose_name
        max_digits = price_field.max_digits
        decimal_places = price_field.decimal_places
        self.assertEqual(label, 'price')
        self.assertEqual(max_digits, 12)
        self.assertEqual(decimal_places, 2)

    def test_delete(self):
        """Entries must never be deleted, status must be changed instead."""
        self.bill.delete(modified_by=self.user)
        self.assertEqual(self.bill.status, DELETED)

    def test_str(self):
        """Strin representation of Bills."""
        self.assertEqual(str(self.bill), str(self.bill.number))


class ReceiptTestCase(TestCase):
    """Tests receipts behaviour."""

    def setUp(self):
        """Set up Data."""
        profile_model = get_model(settings.AUTH_USER_MODEL)
        self.user = profile_model.objects.create(username='cuser')
        currency_model = get_model(settings.CURRENCY_MODEL)
        self.currency = currency_model.objects.create(
            code='usd',
            created_by=self.user
        )
        company_model = get_model(settings.COMPANY_MODEL)
        self.company = company_model.objects.create(
            name='Company',
            number='1',
            created_by=self.user
        )
        self.bill = Bill.objects.create(
            number=1,
            purchase_date=timezone.now(),
            company=self.company,
            currency=self.currency,
            price=0,
            created_by=self.user,
        )
        self.receipt = Receipt.objects.create(
            number=1,
            bill=self.bill,
        )

    def test_fields(self):
        """Make sure the model has the following fields."""
        number_field = self.receipt._meta.get_field('number')
        label = number_field.verbose_name
        max_field = number_field.max_length
        self.assertEqual(label, 'number')
        self.assertEqual(max_field, 20)
        bill_field = self.receipt._meta.get_field('bill')
        label = bill_field.verbose_name
        # on_delete = bill_field.on_delete
        self.assertEqual(label, 'bill')
        # self.assertEqual(on_delete, models.DO_NOTHING)


class ProductTestCase(TestCase):
    """Test the behaviour of products in purchases module."""

    def setUp(self):
        """Set up Data."""
        profile_model = get_model(settings.AUTH_USER_MODEL)
        self.user = profile_model.objects.create(
            username='cuser'
        )
        currency_model = get_model(settings.CURRENCY_MODEL)
        self.currency = currency_model.objects.create(
            code='usd',
            created_by=self.user
        )
        company_model = get_model(settings.COMPANY_MODEL)
        self.company = company_model.objects.create(
            name='Company',
            number='1',
            created_by=self.user
        )
        self.bill = Bill.objects.create(
            number=1,
            purchase_date=timezone.now(),
            company=self.company,
            currency=self.currency,
            price=0,
            created_by=self.user,
        )
        self.receipt = Receipt.objects.create(
            number=1,
            bill=self.bill,
        )
        self.product = Product.objects.create(
            serial_number='1',
            receipt=self.receipt,
            price=0.0,
            created_by=self.user
        )

    def test_labels(self):
        """Test labels."""
        receipt_label = self.product._meta.get_field('receipt')
        label = receipt_label.verbose_name
        self.assertEqual(label, 'receipt')
        label = self.product._meta.get_field('price').verbose_name
        self.assertEqual(label, 'price')

    def test_str(self):
        """Test the strin representation of the product."""
        self.assertEqual(str(self.product), str(self.product.serial_number))

    def test_creates_warehouse_product(self):
        """Tests the interaction between purchases and warehouse.

        This test is no longer relevant, This module should not care if there
        is a warehouse module.
        """
        try:
            warehouse_model = get_model(settings.WAREHOUSE_PRODUCT_MODEL)
            w_p = warehouse_model.objects.get(serial_number='1')
            self.assertEqual(self.product.serial_number, w_p.serial_number)
        except ImproperlyConfigured:
            self.fail("Failed when tried to get a warehouseproduct model.")
        except warehouse_model.DoesNotExist:
            self.fail("purchases.product.create() should create "
                      "a warehouse product.")
        except Exception:
            self.fail("purchases_product.create() raised an "
                      "unexpected exception.")

        # change the settings and try to create a model. should be able to
        # create a purchase product, but no warehouse product.
        # settings.WAREHOUSE_PRODUCT_MODEL = 'uknown_app.model'
        try:
            product = Product.objects.create(
                serial_number='2',
                receipt=self.receipt,
                price=1,
                created_by=self.user
            )
        except Exception:
            self.fail("purchases_product.craete() raised an "
                      "unexpected exception.")
        self.assertEqual(product.serial_number, str(product))
