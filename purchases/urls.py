"""Urls for the purchases app."""
from django.urls import path

from .views import (
    BillCreateView,
    BillListView,
    BillUpdateView,
    ProductListView,
    ProductCreateView,
    ProductUpdateView
)

app_name = 'purchases'

urlpatterns = [
    path('bill/add/', BillCreateView.as_view(), name='bill_add'),
    path('bills/', BillListView.as_view(), name='bills'),
    path(
        'bills/<slug:number>',
        BillUpdateView.as_view(),
        name='bill_update'
    ),
    path(
        'bills/<slug:receipt_number>/products',
        ProductListView.as_view(),
        name='products'
    ),
    path(
        'receipts/<slug:receipt_number>/product/add',
        ProductCreateView.as_view(),
        name='product_add'
    ),
    path(
        'receipts/<slug:number>/product/<slug:serial_number>',
        ProductUpdateView.as_view(),
        name='product_update'
    ),
]
