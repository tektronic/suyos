"""Views for the purchases module."""
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    PermissionRequiredMixin
    )
from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse_lazy

from core.views import get_next_url

from .models import Bill, Product, Receipt
from .forms import BillForm, ProductForm, CreateProductsForm


class BillUpdateView(PermissionRequiredMixin, UpdateView):
    """Updating a Bill."""

    permission_required = (
        'purchases.change_bill'
    )
    form_class = BillForm
    template_name = 'purchases/bill_form.html'
    context_object_name = 'bill'
    model = Bill
    title = None
    slug_field = 'number'
    slug_url_kwarg = 'number'

    def get_success_url(self):
        """Get correct url on successful creation of a bill."""
        next_url, kwargs = get_next_url(self.request.GET.get('next', None))
        if not next_url:
            next_url = 'purchases:bills'
        return reverse_lazy(next_url, kwargs=kwargs)

    def get_form_kwargs(self):
        """Get extra information for the form."""
        kwargs = super().get_form_kwargs()
        kwargs['modified_by'] = self.request.user
        bill = get_object_or_404(Bill, number=self.kwargs.get('number'))
        kwargs['bill'] = bill
        return kwargs

    def get_context_data(self, *args, **kwargs):
        """Extra data for the bill form."""
        context = super().get_context_data(*args, **kwargs)
        if self.title:
            context['title'] = self.title
        return context


class BillCreateView(PermissionRequiredMixin, CreateView):
    """Create a bill."""

    permission_required = (
        'purchases.add_bill',
    )
    form_class = BillForm
    template_name = 'purchases/bill_form.html'
    context_object_name = 'bill'
    title = None

    def get_success_url(self):
        """On sucessful creation redirect to correct url."""
        next_url, kwargs = get_next_url(self.request.GET.get('next', None))
        if not next_url:
            next_url = 'purchases:bills'
        return reverse_lazy(next_url, kwargs=kwargs)

    def get_form_kwargs(self):
        """Extra arguments for the form."""
        kwargs = super().get_form_kwargs()
        kwargs['modified_by'] = self.request.user
        return kwargs

    def get_context_data(self, *args, **kwargs):
        """Extra data for the templates."""
        context = super().get_context_data(*args, **kwargs)
        if self.title:
            context['title'] = self.title
        return context


class BillListView(LoginRequiredMixin, ListView):
    """TODO: add permissions."""

    model = Bill
    context_object_name = 'bill_list'
    title = None

    def get_context_data(self, *args, **kwargs):
        """Get extra contex data."""
        context = super().get_context_data(*args, **kwargs)
        context['purchases_ddl'] = 'active'
        context['purchases_bills'] = 'active'
        if self.title:
            context['title'] = self.title
        return context


class ProductUpdateView(PermissionRequiredMixin, UpdateView):
    """Update a single product."""

    permission_required = (
        'purchases.change_product'
    )
    form_class = ProductForm
    template_name = 'purchases/product_update_form.html'
    context_object_name = 'product'
    title = None
    model = Product
    slug_field = 'serial_number'
    slug_url_kwarg = 'serial_number'

    def get_success_url(self):
        """Get success url for products."""
        next_url, kwargs = get_next_url(self.request.GET.get('next', None))
        if not next_url:
            next_url = 'purchases:products'
            kwargs = {'receipt_number': self.kwargs.get('number', None)}
        return reverse_lazy(next_url, kwargs=kwargs)

    def get_form_kwargs(self):
        """Get user id for the init of the form."""
        kwargs = super().get_form_kwargs()
        kwargs['modified_by'] = self.request.user
        return kwargs

    def get_context_data(self, *args, **kwargs):
        """Get extra context data."""
        context = super().get_context_data(*args, **kwargs)
        context['receipt_number'] = self.kwargs.get('number', None)
        if self.title:
            context['title'] = self.title
        return context


class ProductCreateView(PermissionRequiredMixin, CreateView):
    """Create a product."""

    permission_required = (
        'purchases.add_product',
    )
    form_class = CreateProductsForm
    template_name = 'purchases/products_create_form.html'
    context_object_name = 'product'
    title = None

    def get_success_url(self):
        """On success creation of products send them to the bills view."""
        next_url, kwargs = get_next_url(self.request.GET.get('next', None))
        if not next_url:
            next_url = 'purchases:bills'
        return reverse_lazy(next_url, kwargs=kwargs)

    def get_form_kwargs(self):
        """Extra arguments required by the form."""
        kwargs = super().get_form_kwargs()
        kwargs['receipt_number'] = self.kwargs.get('receipt_number')
        kwargs['modified_by'] = self.request.user
        kwargs['serial_number'] = self.kwargs.get('serial_number')
        return kwargs

    def get_context_data(self, *args, **kwargs):
        """Get extra context data for the template."""
        context = super().get_context_data(*args, **kwargs)
        if self.title:
            context['title'] = self.title
        return context


class ProductListView(PermissionRequiredMixin, ListView):
    """Product List View."""

    permission_required = (
        'purchases.add_product',
        'purchases.change_product'
    )
    model = Product
    context_object_name = 'product_list'
    title = None

    def get_queryset(self):
        """Get the products for the receipt."""
        receipt = Receipt.objects.get(number=self.kwargs.get('receipt_number'))
        return Product.objects.filter(receipt=receipt)

    def get_context_data(self, *args, **kwargs):
        """Get context data for the template."""
        context = super().get_context_data(*args, **kwargs)
        context['purchases_ddl'] = 'active'
        context['purchases_products'] = 'active'
        context['receipt_number'] = self.kwargs.get('receipt_number')
        if self.title:
            context['title'] = self.title
        return context
