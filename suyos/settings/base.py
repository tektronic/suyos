"""Django settings for suyos project."""
import os

from django.urls import reverse_lazy

# WARNING: getting languae from an app. this has to use lazy translation
from accounts import LANGUAGES as available_languages, SPANISH


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = os.environ.get('PROJECT_KEY')

SALESTAX_MODEL = 'core.SalesTax'
CURRENCY_MODEL = 'core.Currency'
AUTH_USER_MODEL = 'accounts.Profile'
COMPANY_MODEL = 'accounts.Company'
PURCHASES_BILL_MODEL = 'purchases.Bill'
PURCHASES_PRODUCT_MODEL = 'purchases.Product'
WAREHOUSE_PRODUCT_MODEL = 'warehouse.Product'


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # 'simple_history', breaks when deploying to heroku
    'widget_tweaks',

    'accounts.apps.AccountsConfig',
    'core.apps.CoreConfig',
    'purchases.apps.PurchasesConfig',
    'warehouse.apps.WarehouseConfig',
]

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'suyos.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'suyos.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGES = available_languages
LANGUAGE_CODE = SPANISH

TIME_ZONE = 'America/Lima'

USE_I18N = True

USE_L10N = True

USE_TZ = True


LOGIN_URL = reverse_lazy("accounts:login")
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"
