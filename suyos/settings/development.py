from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite3',
    }
}

MIDDLEWARE.insert(0, 'django.middleware.security.SecurityMiddleware')

DEBUG = True

STATIC_URL = '/static/'

INSTALLED_APPS.extend([
])
