from os.path import join

from .base import *
from dj_database_url import config

DEBUG = False

ALLOWED_HOSTS = [
    'localhost',
    'suyos.herokuapp.com',
    'http://suyos.herokuapp.com'
]

DATABASES = {
    'default': config()
}
DATABASES['default'] = config()

MIDDLEWARE.insert(0, 'whitenoise.middleware.WhiteNoiseMiddleware')
MIDDLEWARE.insert(0, 'django.middleware.security.SecurityMiddleware')

STATIC_ROOT = join(BASE_DIR, 'staticfiles')
STATIC_URL = '/staticfiles/'

INSTALLED_APPS.extend([
    'rest_framework',
    'rest_framework.authtoken',
])

# rest_framework settings
REST_FRAMEWORK_TOKEN_EXPIRE_HOURS = 24
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS':
        'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10,
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'accounts.authentication.ExpiringTokenAuthentication',
    )
}
