''' suyos main URL Configuration
'''
from django.urls import include, path
from django.contrib import admin

urlpatterns = [
    path('accounts/', include('accounts.urls')),
    path('purchases/', include('purchases.urls')),
    path('admin/', admin.site.urls),
    path('', include('core.urls')),
]
