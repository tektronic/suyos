from django.utils.translation import ugettext_lazy as _
from core import (
    DESKTOP, LAPTOP,
    PROCESSOR, MEMORY, HDD, MAINBOARD, CDD, CASE, TONER,
)

default_app_config = 'suyos.warehouse.apps.WarehouseConfig'

AVAILABLE = 'available'
RENTED = 'rented'
DISASSEMBLED = 'disassembled'
SOLD = 'sold'
DAMAGED = 'damaged'
FIXING = 'fixing'
IN_COMPLEX = 'in_complex'
PURCHASED = 'purchased'
STATUSES = {
    PURCHASED   : _('Purchased'),
    AVAILABLE   : _('Available'),
    RENTED      : _('Rented'),
    DISASSEMBLED: _('Disassembled'),
    SOLD        : _('Sold'),
    DAMAGED     : _('Damaged'),
    FIXING      : _('Fixing'),
    IN_COMPLEX  : _('In Complex'),
}

SERIAL = 'serial'
LASER = 'laser'
TONER = 'toner'
PRINTER_TECHNOLOGIES = {
    SERIAL    : _("Serial"),
    LASER     : _("Laser"),
    TONER     : _("Toner"),
}

WIRED = 'wired'
WIRELESS = 'wireless'
BLUETHOOTH = 'bluethoot'
PERIPHERAL_CONNECTIONS = {
    WIRED  : _("Wired"),
    WIRELESS  : _("Wireless"),
    BLUETHOOTH  : _("Bluetooth"),
}

LED = 'led'
LCD = 'lcd'
CTR = 'ctr'
SCREEN_TECHNOLOGIES = {
    LED: _("LED"),
    LCD: _("LCD"),
    CTR: _("CTR"),
}
SIZE_11 = '11'
SIZE_13 = '13'
SIZE_15 = '15'
SIZE_17 = '17'
SIZE_19 = '19'
SIZE_21 = '21'
SCREEN_SIZES = {
    SIZE_11    : "11'",
    SIZE_13    : "13'",
    SIZE_15    : "15'",
    SIZE_17    : "17'",
    SIZE_19    : "19'",
    SIZE_21    : "21'",
}
SSD = 'ssd'
HDD = 'hdd'
HYBRID = 'hybrid'
HDD_TECHNOLOGIES = {
    SSD   : _("SSD"),
    HDD   : _("HDD"),
    HYBRID: _("Hybrid"),
}

COMPUTERS = {
    LAPTOP  : _('Laptop'),
    DESKTOP : _('Desktop')
}

PART = 'part'
CONSUMABLE = 'consumable'
UNKNOWN = 'unknown'
PIECE_TYPES = {
    PART : _('Part'),
    CONSUMABLE : _('Consumable'),
    UNKNOWN : _('Unknown'),
}
PART_TYPES = [
    PROCESSOR, MEMORY, HDD, MAINBOARD, CDD, CASE,
]
CONSUMABLE_TYPES = [ TONER, ]
