from django.apps import AppConfig


class WarehouseConfig(AppConfig):
    name = 'warehouse'

    def ready(self):
        ''' '''
        from .signals import create_from_purchased_product
