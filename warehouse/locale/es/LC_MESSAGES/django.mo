��    !      $  /   ,      �     �  	     )        8  
   Q     \     d     q  5   x  
   �     �  	   �     �     �  "   �     �          6     ;     C  	   L  
   V     a  	   }  
   �  	   �     �     �     �     �     �  &   �  k  �     h  
   �  1   �      �  
   �  	   �  	   �  
   �  *   
     5     A     I     R     [  *   c     �  "   �     �  	   �  
   �  
   �     �       	   !     +     7  	   @     J     R     ^     r     �           !                                                                              
   	                                                                   A product must be provided Available Can't return a product that is Available. Company is not a renter. Consumable Damaged Disassembled Fixing Given product details are not specified in the system In Complex Keyboard Keyboards Memories Memory Must provide a company to rent to. Need to give tektronik id Need to relate to a product. Part Printer Printers Processor Processors Product already has details Projector Projectors Purchased Rented Sold Unknown Warehouse Product Warehouse Products the given product is not a {} nor a {} Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-13 13:36-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Debes incluir un producto Disponible No pudo regresar un producto que esta disponible. La compañia no es un alquilador Consumible Malogrado Desarmado Arreglando Los productos dados no estan en el sistema En Complejo Teclado Teclados Memorias Memoria Debes proveer una compañía para alquilar Necesitas dar un tektronic id Necesitas relacionar a un producto Parte Impresora Impresoras Procesador Procesadores Producto ya tiene informacion. Projector Projectores Comprado Alquilado Vendido Desconocido Producto de Almacen Productos de Almacen El producto not es {} o {} 