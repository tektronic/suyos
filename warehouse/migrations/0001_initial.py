# Generated by Django 2.0 on 2018-08-11 09:54

from decimal import Decimal
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Case',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power', models.PositiveIntegerField(blank=True, default=None, null=True)),
            ],
            options={
                'verbose_name': 'Case',
                'verbose_name_plural': 'Casess',
            },
        ),
        migrations.CreateModel(
            name='Cdd',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('speed', models.PositiveIntegerField(blank=True, default=None, null=True)),
                ('can_write', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'CD Drive',
                'verbose_name_plural': 'CD Drives',
            },
        ),
        migrations.CreateModel(
            name='Desktop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tektronic_id', models.CharField(max_length=12, unique=True)),
            ],
            options={
                'verbose_name': 'Desktop',
                'verbose_name_plural': 'Desktops',
            },
        ),
        migrations.CreateModel(
            name='Hdd',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kind', models.CharField(choices=[('desktop', 'Desktop'), ('laptop', 'Laptop')], max_length=7)),
                ('technology', models.CharField(blank=True, choices=[('hdd', 'HDD'), ('hybrid', 'Hybrid'), ('ssd', 'SSD')], default=None, max_length=10, null=True)),
                ('capacity', models.DecimalField(blank=True, decimal_places=3, default=None, max_digits=6, null=True, validators=[django.core.validators.MinValueValidator(Decimal('0.000'))])),
                ('rpm', models.IntegerField(blank=True, default=None, null=True)),
            ],
            options={
                'verbose_name': 'Hard Disk Drive',
                'verbose_name_plural': 'Hard Disk Drives',
            },
        ),
        migrations.CreateModel(
            name='Keyboard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tektronic_id', models.CharField(max_length=12, unique=True)),
                ('connection', models.CharField(blank=True, choices=[('bluethoot', 'Bluetooth'), ('wired', 'Wired'), ('wireless', 'Wireless')], default=None, max_length=10, null=True)),
            ],
            options={
                'verbose_name': 'Keyboard',
                'verbose_name_plural': 'Keyboards',
            },
        ),
        migrations.CreateModel(
            name='Laptop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tektronic_id', models.CharField(max_length=12, unique=True)),
                ('screen_size', models.CharField(blank=True, choices=[('11', "11'"), ('13', "13'"), ('15', "15'"), ('17', "17'"), ('19', "19'"), ('21', "21'")], default=None, max_length=10, null=True)),
            ],
            options={
                'verbose_name': 'Laptop',
                'verbose_name_plural': 'Laptops',
            },
        ),
        migrations.CreateModel(
            name='Mainboard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('socket', models.CharField(blank=True, default=None, max_length=20, null=True)),
            ],
            options={
                'verbose_name': 'Mainboard',
                'verbose_name_plural': 'Mainboards',
            },
        ),
        migrations.CreateModel(
            name='Memory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kind', models.CharField(choices=[('desktop', 'Desktop'), ('laptop', 'Laptop')], max_length=7)),
                ('technology', models.CharField(blank=True, default=None, max_length=20, null=True)),
                ('capacity', models.DecimalField(blank=True, decimal_places=2, default=None, max_digits=6, null=True, validators=[django.core.validators.MinValueValidator(Decimal('0.00'))])),
            ],
            options={
                'verbose_name': 'Memory',
                'verbose_name_plural': 'Memories',
            },
        ),
        migrations.CreateModel(
            name='Monitor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tektronic_id', models.CharField(max_length=12, unique=True)),
                ('technology', models.CharField(blank=True, choices=[('ctr', 'CTR'), ('lcd', 'LCD'), ('led', 'LED')], default=None, max_length=10, null=True)),
                ('size', models.CharField(blank=True, choices=[('11', "11'"), ('13', "13'"), ('15', "15'"), ('17', "17'"), ('19', "19'"), ('21', "21'")], default=None, max_length=5, null=True)),
            ],
            options={
                'verbose_name': 'Monitor',
                'verbose_name_plural': 'Monitors',
            },
        ),
        migrations.CreateModel(
            name='Mouse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tektronic_id', models.CharField(max_length=12, unique=True)),
                ('connection', models.CharField(blank=True, choices=[('bluethoot', 'Bluetooth'), ('wired', 'Wired'), ('wireless', 'Wireless')], default=None, max_length=10, null=True)),
            ],
            options={
                'verbose_name': 'Mouse',
                'verbose_name_plural': 'Mouses',
            },
        ),
        migrations.CreateModel(
            name='Piece',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modified', models.DateTimeField(auto_now=True)),
                ('type', models.CharField(choices=[('consumable', 'Consumable'), ('part', 'Part'), ('unknown', 'Unknown')], default='unknown', max_length=10)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Printer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tektronic_id', models.CharField(max_length=12, unique=True)),
                ('technology', models.CharField(blank=True, choices=[('laser', 'Laser'), ('serial', 'Serial'), ('toner', 'Toner')], default=None, max_length=10, null=True)),
                ('connection', models.CharField(blank=True, choices=[('bluethoot', 'Bluetooth'), ('wired', 'Wired'), ('wireless', 'Wireless')], default=None, max_length=10, null=True)),
                ('is_multifunctional', models.NullBooleanField(default=False)),
                ('ppm', models.IntegerField(blank=True, default=None, null=True, verbose_name='ppm')),
            ],
            options={
                'verbose_name': 'Printer',
                'verbose_name_plural': 'Printers',
            },
        ),
        migrations.CreateModel(
            name='Processor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('generation', models.CharField(blank=True, default=None, max_length=20, null=True)),
                ('speed', models.DecimalField(blank=True, decimal_places=2, default=None, max_digits=6, null=True, validators=[django.core.validators.MinValueValidator(Decimal('0.00'))])),
                ('piece', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Piece')),
            ],
            options={
                'verbose_name': 'Processor',
                'verbose_name_plural': 'Processors',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('serial_number', models.CharField(max_length=50, unique=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('brand', models.CharField(blank=True, max_length=50, null=True)),
                ('model', models.CharField(blank=True, max_length=50, null=True)),
                ('status', models.CharField(choices=[('available', 'Available'), ('damaged', 'Damaged'), ('disassembled', 'Disassembled'), ('fixing', 'Fixing'), ('in_complex', 'In Complex'), ('purchased', 'Purchased'), ('rented', 'Rented'), ('sold', 'Sold')], default='available', max_length=10)),
                ('return_date', models.DateField(null=True)),
                ('end_warranty_date', models.DateField(default=django.utils.timezone.now)),
                ('type', models.CharField(choices=[('case', 'Case'), ('cdd', 'CD Drive'), ('desktop', 'Desktop'), ('hdd', 'Hard Disk'), ('keyboard', 'Keyboard'), ('laptop', 'Laptop'), ('mainboard', 'Mainboard'), ('memory', 'Memory'), ('monitor', 'Monitor'), ('mouse', 'Mouse'), ('printer', 'Printer'), ('processor', 'Processor'), ('projector', 'Projector'), ('toner', 'Toner'), ('unknown', 'Unknown')], default='unknown', max_length=10)),
                ('modified_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('rented_to', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='accounts.Company')),
            ],
            options={
                'verbose_name': 'Warehouse Product',
                'verbose_name_plural': 'Warehouse Products',
            },
        ),
        migrations.CreateModel(
            name='Projector',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tektronic_id', models.CharField(max_length=12, unique=True)),
                ('brightness', models.PositiveIntegerField(blank=True, default=None, null=True)),
                ('resolution_width', models.PositiveIntegerField(blank=True, default=None, null=True)),
                ('resolution_height', models.PositiveIntegerField(blank=True, default=None, null=True)),
                ('product', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Product')),
                ('rented_from', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.Company')),
            ],
            options={
                'verbose_name': 'Projector',
                'verbose_name_plural': 'Projectors',
            },
        ),
        migrations.CreateModel(
            name='Toner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('piece', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Piece')),
            ],
            options={
                'verbose_name': 'Toner',
                'verbose_name_plural': 'Toners',
            },
        ),
        migrations.AddField(
            model_name='printer',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Product'),
        ),
        migrations.AddField(
            model_name='printer',
            name='rented_from',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.Company'),
        ),
        migrations.AddField(
            model_name='piece',
            name='comes_from',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='piece_created_parts', to='warehouse.Product'),
        ),
        migrations.AddField(
            model_name='piece',
            name='is_in',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='piece_parts', to='warehouse.Product'),
        ),
        migrations.AddField(
            model_name='piece',
            name='modified_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='piece',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Product'),
        ),
        migrations.AddField(
            model_name='mouse',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Product'),
        ),
        migrations.AddField(
            model_name='mouse',
            name='rented_from',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.Company'),
        ),
        migrations.AddField(
            model_name='monitor',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Product'),
        ),
        migrations.AddField(
            model_name='monitor',
            name='rented_from',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.Company'),
        ),
        migrations.AddField(
            model_name='memory',
            name='piece',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Piece'),
        ),
        migrations.AddField(
            model_name='mainboard',
            name='piece',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Piece'),
        ),
        migrations.AddField(
            model_name='laptop',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Product'),
        ),
        migrations.AddField(
            model_name='laptop',
            name='rented_from',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.Company'),
        ),
        migrations.AddField(
            model_name='keyboard',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Product'),
        ),
        migrations.AddField(
            model_name='keyboard',
            name='rented_from',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.Company'),
        ),
        migrations.AddField(
            model_name='hdd',
            name='piece',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Piece'),
        ),
        migrations.AddField(
            model_name='desktop',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Product'),
        ),
        migrations.AddField(
            model_name='desktop',
            name='rented_from',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.Company'),
        ),
        migrations.AddField(
            model_name='cdd',
            name='piece',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Piece'),
        ),
        migrations.AddField(
            model_name='case',
            name='piece',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='warehouse.Piece'),
        ),
    ]
