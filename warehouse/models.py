from decimal import Decimal

from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from core import (
    PRODUCTS,
    MONITOR,
    PRINTER,
    PROJECTOR,
    KEYBOARD,
    MOUSE,
    LAPTOP,
    DESKTOP,
    PROCESSOR,
    MEMORY,
    HDD,
    MAINBOARD,
    CDD,
    CASE,
    TONER,
    UNKNOWN,
)
from core.models import AbstractProduct as CoreProduct, Auditable

from . import (
    SCREEN_TECHNOLOGIES,
    SCREEN_SIZES,
    PRINTER_TECHNOLOGIES,
    PERIPHERAL_CONNECTIONS,
    COMPUTERS,
    HDD_TECHNOLOGIES,
    # statuses needed below
    STATUSES,
    AVAILABLE,
    RENTED,
    PIECE_TYPES,
    UNKNOWN as UNKNOWN_PART,
    PART_TYPES,
    CONSUMABLE_TYPES,
    PART,
    CONSUMABLE,
)


class Product(CoreProduct, Auditable):
    """Generic information about a product."""
    brand = models.CharField(max_length=50, null=True, blank=True)
    model = models.CharField(max_length=50, null=True, blank=True)
    status = models.CharField(
        max_length=10,
        choices=sorted(STATUSES.items()),
        default=AVAILABLE
    )
    rented_to = models.ForeignKey(
        settings.COMPANY_MODEL,
        null=True,
        default=None,
        on_delete=models.DO_NOTHING
    )
    return_date = models.DateField(null=True)
    end_warranty_date = models.DateField(default=timezone.now)
    type = models.CharField(
        max_length=10,
        choices=sorted(PRODUCTS.items()),
        default=UNKNOWN
    )

    def rent(self, company, return_date=None, **kwargs):
        ''' Modifies a product to a rented state.
        :company: a company that the product is being rented to
        :return_date: date in which the product is to be returned
        '''
        if company is None:
            raise ValueError(_('Must provide a company to rent to.'))
        elif not company.is_renter:
            raise ValueError(_('Company is not a renter.'))
        self.return_date = return_date
        self.status = RENTED
        self.rented_to = company
        self.save(**kwargs)

    def pickup(self, **kwargs):
        '''
        '''
        if self.status is AVAILABLE:
            raise ValueError(_("Can\'t return a product that is Available."))
        self.company = None
        self.status = AVAILABLE
        self.return_date = None
        self.save(**kwargs)

    def assign_to_other(self, assign_to):
        ''' assigns this product to e part of another product.
        Multiple scenarios:
        * This product is a Pice and can simply assing
        * This product is not a Piece
          - create a new piece as an unknown part.
          - assign
        '''
        pass

    def save(self, *args, **kwargs):
        '''
        '''
        return super(Product, self).save(*args, **kwargs)

    def get_details(self):
        ''' Gets details of a product based on the type field.
        '''
        if self.type == UNKNOWN:
            return None

        elif self.type == MONITOR:
            return self.monitor
        elif self.type == PRINTER:
            return self.printer
        elif self.type == PROJECTOR:
            return self.projector
        elif self.type == KEYBOARD:
            return self.keyboard
        elif self.type == MOUSE:
            return self.mouse
        elif self.type == LAPTOP:
            return self.laptop
        elif self.type == DESKTOP:
            return self.desktop

        elif self.type == PROCESSOR:
            return self.piece.processor
        elif self.type == MEMORY:
            return self.piece.memory
        elif self.type == HDD:
            return self.piece.hdd
        elif self.type == MAINBOARD:
            return self.piece.mainboard
        elif self.type == CDD:
            return self.piece.cdd
        elif self.type == CASE:
            return self.piece.case

        elif self.type == TONER:
            return self.piece.toner

        else:
            raise ValueError(_(
                'Given product details are not specified in the system'
            ))

    def __str__(self):
        return str(self.serial_number)

    class Meta:
        verbose_name = _('Warehouse Product')
        verbose_name_plural = _('Warehouse Products')


class RentableProductManager(models.Manager):
    """Manager for all rentable products."""

    def create(self, *args, **kwargs):
        ''' Creating a rentable product.
        * tektronic id must be given. this number will be set on the
          rentable product and on the warehouse product it relates to.
        * product is a product this Rentable product would be linked to.
          the type of the product would be the class name of the 'concrete'
          product that would be created.
        '''
        tektronic_id = kwargs.get('tektronic_id')
        product = kwargs.get('product')
        if not tektronic_id:
            raise ValueError(_('Need to give tektronik id'))
        if not product:
            raise ValueError(_('Need to relate to a product.'))
        if product.type is not UNKNOWN:
            raise ValueError(_('Product already has details'))
        created_by = kwargs.pop('created_by', None)
        detail = super(RentableProductManager, self).create(*args, **kwargs)
        product.tektronic_id = tektronic_id
        product.type = detail.__class__.__name__.lower()
        product.save(modified_by=created_by)
        return detail


class RentableProduct(models.Model):
    """Defines products that are purchased by the company.

    :tektronic_id: an id given by the company. If not used, remove
    :rented_from: a company from which this product was rented from.
    :product: a warehouse product.
    """
    tektronic_id = models.CharField(max_length=12, unique=True)
    rented_from = models.ForeignKey(
        settings.COMPANY_MODEL,
        on_delete=models.DO_NOTHING,
        related_name='+',
        null=True,
        blank=True,
        default=None,
    )
    product = models.OneToOneField(
        settings.WAREHOUSE_PRODUCT_MODEL,
        on_delete=models.CASCADE
    )

    objects = RentableProductManager()

    class Meta:
        abstract = True


class Piece(Auditable):
    ''' Defines parts. Parts are products that can be part of other Products.
    A part cannot be part of another part.
    '''
    type = models.CharField(
        max_length=10,
        choices=sorted(PIECE_TYPES.items()),
        default=UNKNOWN_PART
    )
    is_in = models.ForeignKey(
        settings.WAREHOUSE_PRODUCT_MODEL,
        on_delete=models.DO_NOTHING,
        related_name='%(class)s_parts',
        null=True,
        default=None
    )
    # used to track if this part came from another product.
    # (i.e a hdd from a brand desktop)
    comes_from = models.ForeignKey(
        settings.WAREHOUSE_PRODUCT_MODEL,
        on_delete=models.DO_NOTHING,
        related_name='%(class)s_created_parts',
        null=True,
        default=None
    )
    product = models.OneToOneField(
        settings.WAREHOUSE_PRODUCT_MODEL,
        on_delete=models.CASCADE
    )

    def __str__(self):
        '''
        '''
        return self.type


# The next 2 classes might be a bit counterintuitive. The abstracPiece classes
# are used by concrete implementation of Pieces (AKA children of the Piece
# model)
class AbstractPieceManager(models.Manager):
    """"""

    def create(self, product, *args, **kwargs):
        """"""
        if not product:
            raise ValueError(_('A product must be provided'))
        created_by = kwargs.pop('created_by', None)
        piece = Piece.objects.create(product=product, created_by=created_by)
        detail_piece = super(AbstractPieceManager, self).create(
            piece=piece, *args, **kwargs
        )
        product_type = detail_piece.__class__.__name__.lower()
        if product_type in PART_TYPES:
            piece.type = PART
        elif product_type in CONSUMABLE_TYPES:
            piece.type = CONSUMABLE
        else:
            detail_piece.delete()  # should delete piece as well...
            raise ValueError(_(
                "the given product is not a {} nor a {}".format(
                    PIECE_TYPES.get(PART),
                    PIECE_TYPES.get(CONSUMABLE),
                )
            ))
        piece.product.type = product_type
        piece.save(modified_by=created_by)
        return detail_piece


class AbstractPiece(models.Model):
    """"""
    piece = models.OneToOneField(Piece, on_delete=models.CASCADE)

    objects = AbstractPieceManager()

    class Meta:
        abstract = True


class Monitor(RentableProduct):
    """"""
    technology = models.CharField(
        max_length=10,
        choices=sorted(SCREEN_TECHNOLOGIES.items()),
        null=True,
        blank=True,
        default=None
    )
    size = models.CharField(
        max_length=5,
        choices=sorted(SCREEN_SIZES.items()),
        null=True,
        blank=True,
        default=None
    )

    class Meta:
        verbose_name = _('Monitor')
        verbose_name_plural = _('Monitors')


class Printer(RentableProduct):
    """"""
    technology = models.CharField(
        max_length=10,
        choices=sorted(PRINTER_TECHNOLOGIES.items()),
        null=True,
        blank=True,
        default=None
    )
    connection = models.CharField(
        max_length=10,
        choices=sorted(PERIPHERAL_CONNECTIONS.items()),
        null=True,
        blank=True,
        default=None
    )
    is_multifunctional = models.NullBooleanField(default=False)
    ppm = models.IntegerField(_('ppm'), null=True, default=None, blank=True)

    class Meta:
        verbose_name = _('Printer')
        verbose_name_plural = _('Printers')


class Projector(RentableProduct):
    """"""
    brightness = models.PositiveIntegerField(
        null=True,
        default=None,
        blank=True
    )
    resolution_width = models.PositiveIntegerField(
        null=True,
        default=None,
        blank=True
    )
    resolution_height = models.PositiveIntegerField(
        null=True,
        default=None,
        blank=True
    )

    class Meta:
        verbose_name = _('Projector')
        verbose_name_plural = _('Projectors')


class Keyboard(RentableProduct):
    """"""
    connection = models.CharField(
        max_length=10,
        choices=sorted(PERIPHERAL_CONNECTIONS.items()),
        null=True,
        blank=True,
        default=None
    )

    class Meta:
        verbose_name = _('Keyboard')
        verbose_name_plural = _('Keyboards')


class Mouse(RentableProduct):
    """"""
    connection = models.CharField(
        max_length=10,
        choices=sorted(PERIPHERAL_CONNECTIONS.items()),
        null=True,
        blank=True,
        default=None
    )

    class Meta:
        verbose_name = _('Mouse')
        verbose_name_plural = _('Mouses')


class Laptop(RentableProduct):
    """"""
    screen_size = models.CharField(
        max_length=10,
        choices=sorted(SCREEN_SIZES.items()),
        null=True,
        blank=True,
        default=None
    )

    class Meta:
        verbose_name = _('Laptop')
        verbose_name_plural = _('Laptops')


class Desktop(RentableProduct):
    """"""

    class Meta:
        verbose_name = _('Desktop')
        verbose_name_plural = _('Desktops')


class Processor(AbstractPiece):
    """"""
    generation = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        default=None
    )
    speed = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        blank=True,
        null=True,
        default=None,
        validators=[MinValueValidator(Decimal("0.00"))]
    )

    class Meta:
        verbose_name = _('Processor')
        verbose_name_plural = _('Processors')


class Memory(AbstractPiece):
    """"""
    kind = models.CharField(
        max_length=7,
        choices=sorted(COMPUTERS.items())
    )
    technology = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        default=None)
    capacity = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        null=True,
        blank=True,
        default=None,
        validators=[MinValueValidator(Decimal("0.00"))]
    )

    class Meta:
        verbose_name = _('Memory')
        verbose_name_plural = _('Memories')


class Hdd(AbstractPiece):
    """"""
    kind = models.CharField(
        max_length=7,
        choices=sorted(COMPUTERS.items())
    )
    technology = models.CharField(
        max_length=10,
        choices=sorted(HDD_TECHNOLOGIES.items()),
        null=True,
        blank=True,
        default=None
    )
    capacity = models.DecimalField(
        max_digits=6,
        decimal_places=3,
        null=True,
        blank=True,
        default=None,
        validators=[MinValueValidator(Decimal("0.000"))]
    )
    rpm = models.IntegerField(null=True, default=None, blank=True)

    class Meta:
        verbose_name = _('Hard Disk Drive')
        verbose_name_plural = _('Hard Disk Drives')


class Cdd(AbstractPiece):
    """"""
    speed = models.PositiveIntegerField(
        null=True,
        default=None,
        blank=True
    )
    can_write = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('CD Drive')
        verbose_name_plural = _('CD Drives')


class Mainboard(AbstractPiece):
    """"""
    socket = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        default=None
    )

    class Meta:
        verbose_name = _('Mainboard')
        verbose_name_plural = _('Mainboards')


class Case(AbstractPiece):
    """"""
    power = models.PositiveIntegerField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = _("Case")
        verbose_name_plural = _('Casess')


class Toner(AbstractPiece):

    class Meta:
        verbose_name = _("Toner")
        verbose_name_plural = _("Toners")
