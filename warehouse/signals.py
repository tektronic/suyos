"""Signals that Warehouse listens to."""
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Product


@receiver(post_save, sender=settings.PURCHASES_PRODUCT_MODEL)
def create_from_purchased_product(sender, instance, created, **kwargs):
    """Create a warehouse product when a purchased product is created."""
    # setting created_by on the model class, is wrong, but haven't found a
    # better solution
    if created:
        created_by = sender.created_by
        serial_number = instance.serial_number
        Product.objects.create(
            created_by=created_by,
            serial_number=serial_number
            )
    else:
        p = Product.objects.get(serial_number=instance.previous_serial_number)
        p.serial_number = instance.serial_number
        p.save()
