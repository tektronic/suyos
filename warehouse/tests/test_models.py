from datetime import timedelta

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError, ImproperlyConfigured
from django.db import DataError, IntegrityError
from django.test import TestCase
from django.utils import timezone

from core import (
    get_model,
    PRINTER,
    MONITOR,
    PROJECTOR,
    KEYBOARD,
    MOUSE,
    LAPTOP,
    DESKTOP,
    PROCESSOR,
    MEMORY, ## add the rest of the products when needed
)
from core.tests.test_models import ModelMixinTestCase

from ..models import *
from .. import PURCHASED, RENTED, AVAILABLE

class ProductTestCase(TestCase):
    ''' tests Warehouse Product '''

    def setUp(self):
        profile_model = get_model(settings.AUTH_USER_MODEL)
        self.user = profile_model.objects.create(username='cuser')
        company_model = get_model(settings.COMPANY_MODEL)
        self.company = company_model.objects.create(
            name='Company',
            number='1',
            created_by=self.user
        )
        self.product = Product.objects.create(
            created_by=self.user,
            serial_number='1',
        )

    def test_warehouse_product_fields(self):
        ''' makes sure that  the required fields are present. '''
        brand_field = self.product._meta.get_field('brand')
        label = brand_field.verbose_name
        max_length = brand_field.max_length
        null = brand_field.null
        blank = brand_field.blank
        self.assertEqual(label, 'brand')
        self.assertEqual(max_length, 50)
        self.assertEqual(null, True)
        self.assertEqual(blank, True)

        model_field = self.product._meta.get_field('model')
        label = model_field.verbose_name
        max_length = model_field.max_length
        null = model_field.null
        blank = model_field.blank
        self.assertEqual(label, 'model')
        self.assertEqual(max_length, 50)
        self.assertEqual(null, True)
        self.assertEqual(blank, True)

        status_field = self.product._meta.get_field('status')
        label = status_field.verbose_name
        max_length = status_field.max_length
        default = status_field.default
        self.assertEqual(label, 'status')
        self.assertEqual(max_length, 10)
        self.assertEqual(default, AVAILABLE)

        company_field = self.product._meta.get_field('rented_to')
        label = company_field.verbose_name
        null = company_field.null
        default = company_field.default
        self.assertEqual(label, 'rented to')
        self.assertEqual(null, True)
        self.assertEqual(default, None)

        return_date = self.product._meta.get_field('return_date')
        label = return_date.verbose_name
        null = return_date.null
        self.assertEqual(label, 'return date')
        self.assertEqual(null, True)

        end_warranty_date = self.product._meta.get_field('end_warranty_date')
        label = end_warranty_date.verbose_name
        default = end_warranty_date.default
        self.assertEqual(label, 'end warranty date')
        self.assertEqual(default, timezone.now)

    def test_product_rent(self):
        with self.assertRaises(ValueError):
            self.product.rent(None, modified_by=self.user)
        with self.assertRaises(ValueError):
            self.product.rent(self.company, modified_by=self.user)
        self.company.is_renter = True
        time = timezone.now()
        try:
            self.product.rent(self.company, time, modified_by=self.user) # maybe add some days...
        except Exception:
            self.fail('product rent raised an unexpected exception.')
        self.assertEqual(self.product.status, RENTED)
        self.assertEqual(self.product.rented_to, self.company)
        self.assertEqual(self.product.return_date, time)

    def test_product_pickup(self):
        with self.assertRaises(ValueError):
            self.product.pickup(modified_by=self.user)
        self.company.is_renter = True
        self.product.rent(self.company, timezone.now(), modified_by=self.user)
        self.product.pickup(modified_by=self.user)
        self.assertEqual(self.product.status, AVAILABLE)
        self.assertEqual(self.product.company, None)
        self.assertEqual(self.product.return_date, None)

    def test_get_detail_for_monitor(self):
        ''' test whether you can get details based on a product.
        '''
        self.assertEqual(self.product.get_details(), None)
        monitor = Monitor.objects.create(
            tektronic_id='20170801TK010001',
            product=self.product,
            created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), MONITOR)
        self.assertEqual(
            self.product.tektronic_id,
            product_detail.tektronic_id
        )
        self.assertEqual(product_detail.product, self.product)
        self.product.type = 'hello'
        with self.assertRaises(ValueError):
            self.product.get_details()

    def test_get_detail_for_printer(self):
        ''' test whether you can get details based on a product.
        '''
        self.assertEqual(self.product.get_details(), None)
        printer = Printer.objects.create(
            tektronic_id='20170801TK010001',
            product=self.product,
            created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), PRINTER)
        self.assertEqual(
            self.product.tektronic_id,
            product_detail.tektronic_id
        )
        self.assertEqual(product_detail.product, self.product)
        self.product.type = 'hello'
        with self.assertRaises(ValueError):
            self.product.get_details()

    def test_get_detail_for_projector(self):
        ''' test whether you can get details based on a product.
        '''
        self.assertEqual(self.product.get_details(), None)
        projector = Projector.objects.create(
            tektronic_id='20170801TK010001',
            product=self.product,
            created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), PROJECTOR)
        self.assertEqual(
            self.product.tektronic_id,
            product_detail.tektronic_id
        )
        self.assertEqual(product_detail.product, self.product)
        self.product.type = 'hello'
        with self.assertRaises(ValueError):
            self.product.get_details()

    def test_get_detail_for_keyboard(self):
        ''' test whether you can get details based on a product.
        '''
        self.assertEqual(self.product.get_details(), None)
        keyboard = Keyboard.objects.create(
            tektronic_id='20170801TK010001',
            product=self.product,
            created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), KEYBOARD)
        self.assertEqual(
            self.product.tektronic_id,
            product_detail.tektronic_id
        )
        self.assertEqual(product_detail.product, self.product)
        self.product.type = 'hello'
        with self.assertRaises(ValueError):
            self.product.get_details()

    def test_get_detail_for_mouse(self):
        ''' test whether you can get details based on a product.
        '''
        self.assertEqual(self.product.get_details(), None)
        mounse = Mouse.objects.create(
            tektronic_id='20170801TK010001',
            product=self.product,
            created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), MOUSE)
        self.assertEqual(
            self.product.tektronic_id,
            product_detail.tektronic_id
        )
        self.assertEqual(product_detail.product, self.product)
        self.product.type = 'hello'
        with self.assertRaises(ValueError):
            self.product.get_details()

    def test_get_detail_for_laptop(self):
        ''' test whether you can get details based on a product.
        '''
        self.assertEqual(self.product.get_details(), None)
        laptop = Laptop.objects.create(
            tektronic_id='20170801TK010001',
            product=self.product,
            created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), LAPTOP)
        self.assertEqual(
            self.product.tektronic_id,
            product_detail.tektronic_id
        )
        self.assertEqual(product_detail.product, self.product)
        self.product.type = 'hello'
        with self.assertRaises(ValueError):
            self.product.get_details()

    def test_get_detail_for_desktop(self):
        ''' test whether you can get details based on a product.
        '''
        self.assertEqual(self.product.get_details(), None)
        desktop = Desktop.objects.create(
            tektronic_id='20170801TK010001',
            product=self.product,
            created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), DESKTOP)
        self.assertEqual(
            self.product.tektronic_id,
            product_detail.tektronic_id
        )
        self.assertEqual(product_detail.product, self.product)
        self.product.type = 'hello'
        with self.assertRaises(ValueError):
            self.product.get_details()

    def test_get_detail_for_processor(self):
        ''' Test whether you can get details of a product that is a piece
        '''
        self.assertEqual(self.product.get_details(), None)
        processor = Processor.objects.create(
            self.product, created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), PROCESSOR)
        self.assertEqual(product_detail.piece.product, self.product)

    def test_get_detail_for_memory(self):
        ''' Test whether you can get details of a product that is a piece
        '''
        self.assertEqual(self.product.get_details(), None)
        memory = Memory.objects.create(
            self.product, created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), MEMORY)
        self.assertEqual(product_detail.piece.product, self.product)

    def test_get_detail_for_hdd(self):
        ''' Test whether you can get details of a product that is a piece
        '''
        self.assertEqual(self.product.get_details(), None)
        hdd = Hdd.objects.create(
            self.product, created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), HDD)
        self.assertEqual(product_detail.piece.product, self.product)

    def test_get_detail_for_mainboard(self):
        ''' Test whether you can get details of a product that is a piece
        '''
        self.assertEqual(self.product.get_details(), None)
        mainboard = Mainboard.objects.create(
            self.product, created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), MAINBOARD)
        self.assertEqual(product_detail.piece.product, self.product)

    def test_get_detail_for_case(self):
        ''' Test whether you can get details of a product that is a piece
        '''
        self.assertEqual(self.product.get_details(), None)
        case = Case.objects.create(
            self.product, created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), CASE)
        self.assertEqual(product_detail.piece.product, self.product)

    def test_get_detail_for_toner(self):
        ''' Test whether you can get details of a product that is a piece
        '''
        self.assertEqual(self.product.get_details(), None)
        toner = Toner.objects.create(
            self.product, created_by=self.user
        )
        try:
            product_detail = self.product.get_details()
        except Exception:
            self.fail("product.get_details() raised an unexpected exception")
        self.assertEqual(product_detail.__class__.__name__.lower(), TONER)
        self.assertEqual(product_detail.piece.product, self.product)

    def test_str(self):
        self.assertEqual(str(self.product), self.product.serial_number)

class RentableProductTestCase(ModelMixinTestCase):
    ''' '''
    mixin = RentableProduct

    def setUp(self):
        profile_model = get_model(settings.AUTH_USER_MODEL)
        self.user = profile_model.objects.create(username='cuser')
        company_model = get_model(settings.COMPANY_MODEL)
        self.company = company_model.objects.create(
            name='Company',
            number='1',
            created_by=self.user
        )
        self.product = Product.objects.create(
            created_by=self.user,
            serial_number='1',
        )

    def test_fields(self):
        ''' Ensures the required fields for an abstract product.
        '''
        # note that this will change self.product.type to
        # RentableProduct.__class__.__name__().lower() This is desired behavior
        # since everytime you create a Monitor, Printer, etc. You want the type
        # of the product to be the lower case class name version of the object
        # created.
        self.abstract_product = self.model.objects.create(
            created_by=self.user,
            tektronic_id='1234TK123456',
            product=self.product
        )
        tektronic_field = self.abstract_product._meta.get_field('tektronic_id')
        label = tektronic_field.verbose_name
        max_length = tektronic_field.max_length
        null = tektronic_field.null
        blank = tektronic_field.blank
        self.assertEqual(label, 'tektronic id')
        self.assertEqual(max_length, 12)
        self.assertEqual(null, False)
        self.assertEqual(blank, False)

        rented_from_field = self.abstract_product._meta.get_field('rented_from')
        label = rented_from_field.verbose_name
        null = rented_from_field.null
        blank = rented_from_field.blank
        default = rented_from_field.default
        self.assertEqual(label, 'rented from')
        self.assertEqual(null, True)
        self.assertEqual(blank, True)
        self.assertEqual(default, None)

        product_field = self.abstract_product._meta.get_field('product')
        label = product_field.verbose_name
        self.assertEqual(label, 'product')

    def test_create(self):
        ''' '''
        with self.assertRaises(ValueError):
            # raises value error because there is on tektronic id
            self.model.objects.create(
                created_by=self.user, product=self.product
            )
        with self.assertRaises(ValueError):
            # raises because there is no product
            self.model.objects.create(
                created_by=self.user, tektronic_id='1'
            )
        try:
            self.model.objects.create(
                created_by=self.user,
                tektronic_id='1',
                product=self.product
            )
        except Exception:
            self.fail('AbstractProduct.create() raised an unexpected exception')
